BruDupeFileRemover
==================

Do you have megabytes or gigabytes of duplicate files, often caused by cloud drives like Microsoft OneDrive, Google Drive or Dropbox? This program allows you to quickly and safely remove those or any other duplicate files from a directory and all the subdirectories under that directory.

For safety, it doesn't actually delete the files. Rather it quickly moves them away to a separate directory. When you are satisfied the separate directory is full of worthless files, you can simply delete it.

test

Instructions (see ScreenShot1.png)

1.  Set your Scan directory (D:\\Dropbox on ScreenShot1.png)

-   This is the directory from which you want to remove duplicates.

1.  Set your Move directory (C:\\DropboxDuplicatesToDelete on ScreenShot1.png).

-   You will be warned if this directory is not empty. If you move files to C:\\, for example, it will be very hard to "change your mind" and copy files back to the Scan directory.

1.  When the Aggressive checkbox is **not** checked, only files that "look" like a duplicate based on their filenames will be considered.

-   **You should run the non-aggressive mode first for an initial clean-up. Then run the aggressive mode to find additional duplicates.**

-   These files "look like" duplicates of MyFile.txt:

    -   MyFile (1).txt

    -   MyFile (002).txt

    -   MyFile - Copy.txt

    -   MyFile (1) - Copy.txt

    -   MyFile (1) (3).txt

-   These files do **not** "look like" duplicates of MyFile.txt:

    -   MyFile.tt

    -   MyFile(1).txt

    -   MyFile 1.txt

    -   MyFile (0x02).txt

-   Cloud services often tag duplicates with the computer name. You can list such names in the box below the Aggressive checkbox to add to the "look like" list. In ScreenShot1.png, for example, you see -XPS8700 and -DaleB-Laptop. When a filename (before extension) ends with one of these strings, it will be added to the "maybe duplicate" list.

-   **BUT** a file that looks like a copy will not be added to the duplicates table unless it's contents are byte-for-byte identical to the original file. \* When you later scan with the Aggressive box checked, you are likely to see additional files with names that don't "look like" copies but are byte-for-byte identical. You should review these very carefully and delete them from the table (with the red X at the top of the table) before Move.

-   **Only duplicate files in a single directory will be removed.**

-   **A file in one directory that duplicates a file in another directory will not be removed.**

1.  Press the Scan button when you are ready to scan for duplicates. The results will be listed below. (See ScreenShot2.png)

-   The files that will be moved are in red and have numbers like 1, 2, 3 etc. The files in black (number 0) are the originals and will not be moved.

-   Delete any files from the table that you don't want to be moved. This is **especially important** in aggressive mode, as some software may use identical files with differing names.

1.  When you are satisfied with the list, press the Move button to move the duplicate files from the Scan Directory to the Move Directory.

2.  If you think you've made a mistake, you can easily (but manually) copy the mistakes from the Move Directory back to the Scan Directory.

**You should run the non-aggressive mode first for an initial clean-up. Then run the aggressive mode to find additional duplicates.**

Copyright 2016 Dale A. Brubaker

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE    

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
