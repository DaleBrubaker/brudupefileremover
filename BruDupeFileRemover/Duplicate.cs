﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;
using System.IO;

namespace BruDupeFileRemover
{
    public class Duplicate
    {
        /// <summary>
        /// Extra copy strings are for computer names used for duplicates, like -Desktop1 or -MyLaptop
        /// </summary>
        /// <param name="fi"></param>
        /// <param name="extraCopyStrings"></param>
        /// <param name="aggressive"></param>
        public Duplicate(FileInfo fi, ICollection<string> extraCopyStrings = null, bool aggressive = false)
        {
            FileInfo = fi;
            DupeNumber = -1;
            if (aggressive)
            {
                SplitFileNameAggressive();
            }
            else
            {
                var copyStrings = extraCopyStrings == null ? new List<string>() : new List<string>(extraCopyStrings);
                copyStrings.RemoveAll(string.IsNullOrEmpty);
                if (!copyStrings.Contains(" - Copy"))
                {
                    copyStrings.Add(" - Copy");
                }
                SplitFileName(copyStrings);
            }
        }

        /// <summary>
        /// Gets the fileName up to the copy suffix
        /// </summary>
        public string BaseName { get; private set; }

        /// <summary>
        /// Gets the BaseName with extension
        /// </summary>
        public string BaseNameWithExtension => $"{BaseName}.{Extension}";

        /// <summary>
        /// Gets the suffixes that represent a Windows copy, e.g. " (01)" or " - Copy" or both.
        /// string.Empty if no suffix exists
        /// </summary>
        public string CopySuffixes { get; private set; }

        /// <summary>
        /// Gets the folder name
        /// </summary>
        public string Directory => FileInfo.DirectoryName;

        /// <summary>
        /// Gets or sets the number of this duplication (0 is the first file, which is NOT a duplicate)
        /// </summary>
        public int DupeNumber { get; set; }

        /// <summary>
        /// Gets the extension
        /// </summary>
        public string Extension => FileInfo.Extension;

        /// <summary>
        /// Gets the fileName with extension but without the folder
        /// </summary>
        public FileInfo FileInfo { get; }

        /// <summary>
        /// Gets the fileName with extension but without the folder
        /// </summary>
        public string FileName => FileInfo.Name;

        /// <summary>
        /// Gets the file length
        /// </summary>
        public long Size => FileInfo.Length;

        public override string ToString()
        {
            return $"{DupeNumber} {Size} {FileInfo.FullName}";
        }

        /// <summary>
        /// Given the fileName without extension, split out BaseName and CopySuffixes
        /// </summary>
        /// <param name="copyStrings"></param>
        private void PullCopySuffixesFromBaseName(ICollection<string> copyStrings)
        {
            while (PulledParens() || PulledCopy(copyStrings))
            {
            }
        }

        private bool PulledCopy(ICollection<string> copyStrings)
        {
            foreach (var copyStr in copyStrings)
            {
                var index = BaseName.LastIndexOf(copyStr, StringComparison.Ordinal);
                if (index >= 0)
                {
                    CopySuffixes = copyStr + CopySuffixes;
                    BaseName = BaseName.Substring(0, index);
                    return true;
                }
            }
            return false;
        }

        private bool PulledParens()
        {
            if (BaseName.Length < 1 || BaseName[BaseName.Length - 1] != ')')
            {
                return false;
            }

            // Walk back to the left paren, make sure there are only digits between them, and adjust endOfThisBaseNameLength
            var endOfBaseNameLength = BaseName.Length - 1; // for the right paren
            for (; endOfBaseNameLength > 0; endOfBaseNameLength--)
            {
                var ch = BaseName[endOfBaseNameLength - 1];
                if (char.IsDigit(ch))
                {
                    continue;
                }
                if (ch == '(')
                {
                    endOfBaseNameLength--;
                    break;
                }

                // only digit or '(' is allowed
                return false;
            }
            var chBeforeRightParen = endOfBaseNameLength < 1 ? '\0' : BaseName[endOfBaseNameLength - 1];
            if (chBeforeRightParen != ' ')
            {
                return false;
            }
            endOfBaseNameLength--; // adjust for the space before the left paren
            var copyStr = BaseName.Substring(endOfBaseNameLength);
            CopySuffixes = copyStr + CopySuffixes;
            BaseName = BaseName.Substring(0, endOfBaseNameLength);
            return true;
        }

        /// <summary>
        /// Split FileName without Extension into BaseName and CopySuffixes
        /// BaseName is the name up to the first space, hyphen or left parenthesis
        /// </summary>
        private void SplitFileNameAggressive()
        {
            var nameWithoutExtension = Path.GetFileNameWithoutExtension(FileName);
            if (nameWithoutExtension == null)
            {
                BaseName = "";
                CopySuffixes = "";
                return;
            }
            var splitIndex = -1;
            for (int i = 0; i < nameWithoutExtension.Length; i++)
            {
                var ch = nameWithoutExtension[i];
                if (ch == ' ' || ch == '-' || ch == '(')
                {
                    splitIndex = i;
                    break;
                }
            }
            if (splitIndex >= 0)
            {
                BaseName = nameWithoutExtension.Substring(0, splitIndex);
                CopySuffixes = nameWithoutExtension.Substring(splitIndex);
            }
            else
            {
                BaseName = nameWithoutExtension;
                CopySuffixes = "";
            }
        }

        /// <summary>
        /// Split FileName without Extension into BaseName and CopySuffixes
        /// </summary>
        /// <param name="copyStrings"></param>
        private void SplitFileName(ICollection<string> copyStrings)
        {
            BaseName = Path.GetFileNameWithoutExtension(FileName);
            PullCopySuffixesFromBaseName(copyStrings);
        }
    }
}