Overall algorithm.
	It is extremely fast to get the entire directory using parallel processing, 
		so we do that first in order to know how many directories we'll process and to provide user feedback.
	Then it is also very fast to find all probably-duplicate file names, so we do that next.
	Now the slow part is reading and comparing file contents, to be absolutely certain the files are duplicates.
		We do this in a final process, where we can report progress based on bytes read.
		We expect almost 100% of the files will have the same contents, so we expect to read all of every candidate file.

Future:
	OneDrive et al may do - [COMPUTER_NAME] as a suffix. Maybe I can handle that.




