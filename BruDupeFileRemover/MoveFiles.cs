﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover
{
    public class MoveFiles
    {
        private readonly ILogger _log;
        private readonly string _moveRoot;
        private readonly string _scanRoot;

        /// <summary>
        /// Minimal state in this class!
        /// </summary>
        /// <param name="moveRoot"></param>
        /// <param name="scanRoot"></param>
        /// <param name="logger">Pass in a special logger, such as a null logger for unit testing. Else use the standard log for this class</param>
        public MoveFiles(string moveRoot, string scanRoot, ILogger logger = null)
        {
            _moveRoot = moveRoot;
            _scanRoot = scanRoot;
            if (logger == null)
            {
                _log = new LoggerBruDupeFileRemover(typeof(Scan));
            }
        }

        public static bool DirectoryIsEmpty(string directory)
        {
            if (string.IsNullOrEmpty(directory))
            {
                return true;
            }
            if (!Directory.Exists(directory))
            {
                return true;
            }
            var directories = Directory.GetDirectories(directory);
            if (directories.Length > 0)
            {
                return false;
            }
            var files = Directory.GetFiles(directory);
            if (files.Length > 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Moves each duplicatesToMove file from _scanRoot to _moveRoot, maintaining relative paths.
        /// </summary>
        /// <param name="duplicatesToMove">ONLY includes DupeNumber > 0</param>
        /// <param name="token"></param>
        /// <param name="onProgressNumBytesMoved"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public void MoveDuplicates(IList<Duplicate> duplicatesToMove, CancellationToken token, IProgress<long> onProgressNumBytesMoved)
        {
            foreach (var duplicate in duplicatesToMove)
            {
                token.ThrowIfCancellationRequested();
                Debug.Assert(duplicate.DupeNumber > 0, "Remove duplicates with DupeNumber == 0");
                try
                {
                    var relativePath = duplicate.FileInfo.FullName.Substring(_scanRoot.Length + 1);
                    var to = Path.Combine(_moveRoot, relativePath);
                    var toFi = new FileInfo(to);
                    var toDirectory = toFi.DirectoryName;
                    Debug.Assert(toDirectory != null, "toDirectory != null");
                    if (!Directory.Exists(toDirectory))
                    {
                        Directory.CreateDirectory(toDirectory);
                    }
                    duplicate.FileInfo.MoveTo(to);
                    onProgressNumBytesMoved.Report(duplicate.Size);
                }
                catch (Exception ex)
                {
                    _log.WithCallInfo().Fatal(ex.Message, ex);
                    throw;
                }
            }
            _log.WithCallInfo().Info($"Finished moving {duplicatesToMove.Count} duplicate files");
        }
    }
}
