﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BruDupeFileRemover.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BruDupeFileRemover.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancelled..
        /// </summary>
        internal static string Cancelled {
            get {
                return ResourceManager.GetString("Cancelled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Files are in Move Directory.
        /// </summary>
        internal static string Files_are_in_Move_Directory {
            get {
                return ResourceManager.GetString("Files_are_in_Move_Directory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Working....
        /// </summary>
        internal static string MainForm_BtnMove_ClickAsync_Working {
            get {
                return ResourceManager.GetString("MainForm_BtnMove_ClickAsync_Working", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move directory is not empty! It should be empty to enable easy recovery if problems occur..
        /// </summary>
        internal static string Move_directory_is_not_empty__It_should_be_empty_to_enable_easy_recovery_if_problems_occur {
            get {
                return ResourceManager.GetString("Move_directory_is_not_empty__It_should_be_empty_to_enable_easy_recovery_if_proble" +
                        "ms_occur", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move directory is not empty! It should be empty to enable easy recovery if problems occur. Move anyway?.
        /// </summary>
        internal static string Move_directory_is_not_empty__It_should_be_empty_to_enable_easy_recovery_if_problems_occur__Move_anyway_ {
            get {
                return ResourceManager.GetString("Move_directory_is_not_empty__It_should_be_empty_to_enable_easy_recovery_if_proble" +
                        "ms_occur__Move_anyway_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Working....
        /// </summary>
        internal static string Working {
            get {
                return ResourceManager.GetString("Working", resourceCulture);
            }
        }
    }
}
