﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using BruDupeFileRemover.Config;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover
{
    internal class ConfigBruDupeFileRemover
    {
        private static readonly ILogger Log = new LoggerBruDupeFileRemover(typeof(ConfigBruDupeFileRemover));
        private readonly IConfig _config;
        private readonly string _group;

        public ConfigBruDupeFileRemover(string group)
        {
            _group = group;
            var configIOFile = new ConfigIOFile(Log);
            _config = new Config.Config(configIOFile, Log);
            var root = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var directory = Path.Combine(root, "BruDupeFileRemover");
            directory = Path.Combine(directory, nameof(BruDupeFileRemover));
            _config.Directory = directory;
        }

        public string Directory => _config.Directory;

        public XElement GetConfig(string id)
        {
            return _config.GetConfig(_group, id);
        }

        public T GetConfig<T>(string id, T defaultValue)
        {
            return _config.GetConfig(_group, id, defaultValue);
        }

        public IEnumerable<T> GetConfigEnumerable<T>(string id, IEnumerable<T> defaultValues)
        {
            return _config.GetConfigEnumerable(_group, id, defaultValues);
        }

        public T GetConfigIXmlSerializable<T>(string id, T defaultValue)
            where T : IXmlSerializable, new()
        {
            return _config.GetConfigIXmlSerializable(_group, id, defaultValue);
        }

        public T GetConfigXmlSerializer<T>(string id, T defaultValue)
        {
            return _config.GetConfigXmlSerializer(_group, id, defaultValue);
        }

        public void Load()
        {
            _config.Load(_group);
        }

        public void Load(string fileName)
        {
            _config.Load(_group, fileName);
        }

        public void RegisterTypeConverter(Type type, TypeConverter typeConverter)
        {
            _config.RegisterTypeConverter(type, typeConverter);
        }

        public void RestoreWindow(string id, IntPtr handle)
        {
            _config.RestoreWindow(_group, id, handle);
        }

        public void Save()
        {
            _config.Save(_group);
        }

        public void Save(string fileName)
        {
            _config.Save(_group, fileName);
        }

        public void SaveAll()
        {
            _config.SaveAll();
        }

        public void SaveWindow(string id, IntPtr handle)
        {
            _config.SaveWindow(_group, id, handle);
        }

        public void SetConfig(XElement element)
        {
            _config.SetConfig(_group, element);
        }

        public void SetConfig<T>(string id, T value)
        {
            _config.SetConfig(_group, id, value);
        }

        public void SetConfigEnumerable<T, TU>(string id, TU values)
            where TU : IEnumerable<T>
        {
            _config.SetConfigEnumerable<T, TU>(_group, id, values);
        }

        public void SetConfigIXmlSerializable<T>(string id, T value)
            where T : IXmlSerializable
        {
            _config.SetConfigIXmlSerializable(_group, id, value);
        }

        public void SetConfigXmlSerializer<T>(string id, T value)
        {
            _config.SetConfigXmlSerializer(_group, id, value);
        }

        public override string ToString()
        {
            return $"Config for {_group}";
        }
    }
}
