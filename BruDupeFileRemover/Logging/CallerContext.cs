﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System.Runtime.CompilerServices;

namespace BruDupeFileRemover.Logging
{
    public class CallerContext
    {
        private readonly ILogger _logger;
        private readonly string _memberName;
        private readonly string _sourceFilePath;
        private readonly int _sourceLineNumber;

        public CallerContext(ILogger logger, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            _logger = logger;
            _memberName = memberName;
            _sourceFilePath = sourceFilePath;
            _sourceLineNumber = sourceLineNumber;
        }

        public ILogger Logger
        {
            get
            {
                return _logger;
            }
        }

        public string MemberName
        {
            get
            {
                return _memberName;
            }
        }

        public string SourceFilePath
        {
            get
            {
                return _sourceFilePath;
            }
        }

        public int SourceLineNumber
        {
            get
            {
                return _sourceLineNumber;
            }
        }
    }
}
