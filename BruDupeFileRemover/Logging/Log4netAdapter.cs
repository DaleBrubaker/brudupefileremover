﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System.Linq;

namespace BruDupeFileRemover.Logging
{
    public class Log4netAdapter : ILogger
    {
        private readonly log4net.ILog _adaptee;
        private readonly bool _addCallerInfo;

        /// <summary>
        /// This creates a null logger. No logging will occur
        /// </summary>
        /// <param name="addCallerInfo">true means append member, caller line numer and file to the message</param>
        public Log4netAdapter(bool addCallerInfo = true)
            : this(null, addCallerInfo)
        {
        }

        /// <summary>
        /// Use a log4net.Ilog adapter
        /// </summary>
        /// <param name="adaptee"></param>
        /// <param name="addCallerInfo">true means append member, caller line numer and file to the message</param>
        public Log4netAdapter(log4net.ILog adaptee, bool addCallerInfo = true)
        {
            _adaptee = adaptee;
            _addCallerInfo = addCallerInfo;
        }

        public void Log(LogEntry entry)
        {
            if (_adaptee == null)
            {
                // This is a null logger. Do nothing.
                return;
            }
            var messageAppend = _addCallerInfo ? $" CallInfo: {entry.CallerContext.MemberName} {entry.CallerContext.SourceLineNumber} {entry.CallerContext.SourceFilePath}" : "";
            switch (entry.Level)
            {
                case LogLevel.Debug:
                case LogLevel.DebugFormat:
                    if (_adaptee.IsDebugEnabled)
                    {
                        if (entry.Exception == null)
                        {
                            _adaptee.Debug(entry.Message + messageAppend);
                        }
                        else
                        {
                            _adaptee.Debug(entry.Message + messageAppend, entry.Exception);
                        }
                    }
                    break;
                case LogLevel.Info:
                case LogLevel.InfoFormat:
                    if (_adaptee.IsInfoEnabled)
                    {
                        if (entry.Exception == null)
                        {
                            _adaptee.Info(entry.Message + messageAppend);
                        }
                        else
                        {
                            _adaptee.Info(entry.Message + messageAppend, entry.Exception);
                        }
                    }
                    break;
                case LogLevel.Warn:
                case LogLevel.WarnFormat:
                    if (_adaptee.IsWarnEnabled)
                    {
                        if (entry.Exception == null)
                        {
                            _adaptee.Warn(entry.Message + messageAppend);
                        }
                        else
                        {
                            _adaptee.Warn(entry.Message + messageAppend, entry.Exception);
                        }
                    }
                    break;
                case LogLevel.Error:
                case LogLevel.ErrorFormat:
                    if (_adaptee.IsErrorEnabled)
                    {
                        if (entry.Exception == null)
                        {
                            _adaptee.Error(entry.Message + messageAppend);
                        }
                        else
                        {
                            _adaptee.Error(entry.Message + messageAppend, entry.Exception);
                        }
                    }
                    break;
                case LogLevel.Fatal:
                case LogLevel.FatalFormat:
                    if (_adaptee.IsFatalEnabled)
                    {
                        if (entry.Exception == null)
                        {
                            _adaptee.Fatal(entry.Message + messageAppend);
                        }
                        else
                        {
                            _adaptee.Fatal(entry.Message + messageAppend, entry.Exception);
                        }
                    }
                    break;
            }
        }

        public void LogFormat(LogEntryFormat entry)
        {
            if (_adaptee == null)
            {
                // This is a null logger. Do nothing.
                return;
            }
            var args = entry.Args.ToArray();
            var messageAppend = _addCallerInfo ? $" CallInfo: {entry.CallerContext.MemberName} {entry.CallerContext.SourceLineNumber} {entry.CallerContext.SourceFilePath}" : "";
            switch (entry.Level)
            {
                case LogLevel.Debug:
                case LogLevel.DebugFormat:
                    if (_adaptee.IsDebugEnabled)
                    {
                        _adaptee.DebugFormat(entry.Format + messageAppend, args);
                    }
                    break;
                case LogLevel.Info:
                case LogLevel.InfoFormat:
                    if (_adaptee.IsInfoEnabled)
                    {
                        _adaptee.InfoFormat(entry.Format + messageAppend, args);
                    }
                    break;
                case LogLevel.Warn:
                case LogLevel.WarnFormat:
                    if (_adaptee.IsWarnEnabled)
                    {
                        _adaptee.WarnFormat(entry.Format + messageAppend, args);
                    }
                    break;
                case LogLevel.Error:
                case LogLevel.ErrorFormat:
                    if (_adaptee.IsErrorEnabled)
                    {
                        _adaptee.ErrorFormat(entry.Format + messageAppend, args);
                    }
                    break;
                case LogLevel.Fatal:
                case LogLevel.FatalFormat:
                    if (_adaptee.IsFatalEnabled)
                    {
                        _adaptee.FatalFormat(entry.Format + messageAppend, args);
                    }
                    break;
            }
        }
    }
}
