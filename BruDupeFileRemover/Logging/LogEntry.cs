﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using EnsureThat;

namespace BruDupeFileRemover.Logging
{
    public class LogEntry
    {
        private readonly CallerContext _callerContext;
        private readonly Exception _exception;
        private readonly LogLevel _level;
        private readonly string _message;

        public LogEntry(CallerContext callerContext, LogLevel level, string message, Exception exception = null)
        {
            Ensure.That(callerContext).IsNotNull();
            Ensure.That(message).IsNotNullOrEmpty();
            _callerContext = callerContext;
            _level = level;
            _message = message;
            _exception = exception;
        }

        public Exception Exception
        {
            get
            {
                return _exception;
            }
        }

        public LogLevel Level
        {
            get
            {
                return _level;
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }
        }

        public CallerContext CallerContext
        {
            get
            {
                return _callerContext;
            }
        }
    }
}