﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Diagnostics;
using System.IO;
using BruDupeFileRemover.Exceptions;
using log4net.Config;

namespace BruDupeFileRemover.Logging
{
    public class Log4NetLogger : Log4netAdapter
    {
        private static bool isConfigured;

        /// <summary>
        /// Specify a name. Use MethodBase.GetCurrentMethod().DeclaringType to get the current class name
        /// </summary>
        /// <param name="className"></param>
        public Log4NetLogger(string className)
            : this(className, null)
        {
        }

        /// <summary>
        /// Specify a name. Use MethodBase.GetCurrentMethod().DeclaringType to get the current class name
        /// </summary>
        /// <param name="className"></param>
        /// <param name="log4netConfigPath">if null, uses the app.config configuration. If non-null, uses the config file specified and watches it during runtime (make sure it's unique in the executing directory)</param>
        public Log4NetLogger(string className, string log4netConfigPath)
            : base(log4net.LogManager.GetLogger(className))
        {
            ConfigureIfNeeded(log4netConfigPath);
        }

        /// <summary>
        /// Get a log4net logger based on the type being logged
        /// </summary>
        /// <param name="type"></param>
        public Log4NetLogger(Type type)
            : this(type, null)
        {
        }

        /// <summary>
        /// Get a log4net logger based on the type being logged
        /// </summary>
        /// <param name="type"></param>
        /// <param name="log4netConfigPath">if null, uses the app.config configuration. If non-null, uses the config file specified and watches it during runtime (make sure it's unique in the executing directory)</param>
        public Log4NetLogger(Type type, string log4netConfigPath)
            : base(log4net.LogManager.GetLogger(type))
        {
            ConfigureIfNeeded(log4netConfigPath);
        }

        private static void ConfigureIfNeeded(string log4netConfigPath)
        {
            var appName = Process.GetCurrentProcess().ProcessName; // debug
            if (!isConfigured)
            {
                if (string.IsNullOrEmpty(log4netConfigPath))
                {
                    XmlConfigurator.Configure();
                }
                else
                {
                    if (!File.Exists(log4netConfigPath))
                    {
                        throw new BruDupeFileRemoverException($"Missing file {log4netConfigPath} for {appName}");
                    }
                    XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netConfigPath));
                }
                isConfigured = true;
            }
        }
    }
}
