﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System.Runtime.CompilerServices;

namespace BruDupeFileRemover.Logging
{
    public static class LoggerExtensions
    {
        public static CallerContext WithCallInfo(this ILogger logger, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            return new CallerContext(logger, memberName, sourceFilePath, sourceLineNumber);
        }
    }
}
