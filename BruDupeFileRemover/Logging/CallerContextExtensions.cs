﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;

namespace BruDupeFileRemover.Logging
{
    public static class CallerContextExtensions
    {
        public static void Debug(this CallerContext callerContext, string message, Exception exception = null)
        {
            callerContext.Logger?.Log(new LogEntry(callerContext, LogLevel.Debug, message, exception));
        }

        public static void Info(this CallerContext callerContext, string message, Exception exception = null)
        {
            callerContext.Logger?.Log(new LogEntry(callerContext, LogLevel.Info, message, exception));
        }

        public static void Warn(this CallerContext callerContext, string message, Exception exception = null)
        {
            callerContext.Logger?.Log(new LogEntry(callerContext, LogLevel.Warn, message, exception));
        }

        public static void Error(this CallerContext callerContext, string message, Exception exception = null)
        {
            callerContext.Logger?.Log(new LogEntry(callerContext, LogLevel.Error, message, exception));
        }

        public static void Fatal(this CallerContext callerContext, string message, Exception exception = null)
        {
            callerContext.Logger?.Log(new LogEntry(callerContext, LogLevel.Fatal, message, exception));
        }

        public static void Debug(this CallerContext callerContext, string format, params object[] args)
        {
            callerContext.Logger?.LogFormat(new LogEntryFormat(callerContext, LogLevel.DebugFormat, format, args));
        }

        public static void Info(this CallerContext callerContext, string format, params object[] args)
        {
            callerContext.Logger?.LogFormat(new LogEntryFormat(callerContext, LogLevel.InfoFormat, format, args));
        }

        public static void Warn(this CallerContext callerContext, string format, params object[] args)
        {
            callerContext.Logger?.LogFormat(new LogEntryFormat(callerContext, LogLevel.WarnFormat, format, args));
        }

        public static void Error(this CallerContext callerContext, string format, params object[] args)
        {
            callerContext.Logger?.LogFormat(new LogEntryFormat(callerContext, LogLevel.ErrorFormat, format, args));
        }

        public static void Fatal(this CallerContext callerContext, string format, params object[] args)
        {
            callerContext.Logger?.LogFormat(new LogEntryFormat(callerContext, LogLevel.FatalFormat, format, args));
        }
    }
}
