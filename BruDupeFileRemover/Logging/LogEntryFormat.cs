﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EnsureThat;

namespace BruDupeFileRemover.Logging
{
    public class LogEntryFormat
    {
        private readonly CallerContext _callerContext;
        private readonly LogLevel _level;
        private readonly string _format;
        private readonly IReadOnlyCollection<object> _args;

        public LogEntryFormat(CallerContext callerContext, LogLevel level, string format, params object[] args)
        {
            Ensure.That(callerContext).IsNotNull();
            Ensure.That(format).IsNotNullOrEmpty();
            Ensure.That(args).IsNotNull().HasItems();
            _callerContext = callerContext;
            _level = level;
            _format = format;
            _args = new ReadOnlyCollection<object>(args);
        }

        public IReadOnlyCollection<object> Args
        {
            get
            {
                return _args;
            }
        }

         public string Format
        {
            get
            {
                return _format;
            }
        }

        public LogLevel Level
        {
            get
            {
                return _level;
            }
        }

        public CallerContext CallerContext
        {
            get
            {
                return _callerContext;
            }
        }
    }
}