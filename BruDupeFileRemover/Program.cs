﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Windows.Forms;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover
{
#pragma warning disable SA1400 // Access modifier must be declared
    static class Program
    {
        private static readonly ILogger Log = 
            new LoggerBruDupeFileRemover(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            using (var mainForm = new MainForm())
            {
                try
                {
                    Application.Run(mainForm);
                }
                catch (Exception ex)
                {
                    Log.WithCallInfo().Fatal(ex.Message, ex);
                }
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                var ex = (Exception)e.ExceptionObject;
                var msg = $"Unhandled exception {ex.Message}";
                Log?.WithCallInfo().Fatal(msg, ex);
            }
            catch (Exception ex)
            {
                var msg = $"Unhandled exception {ex.Message}";
                Log?.WithCallInfo().Fatal(msg, ex);
                throw;
            }
        }
    }
}
