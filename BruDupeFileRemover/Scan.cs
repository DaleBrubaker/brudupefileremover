﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BruDupeFileRemover.Exceptions;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover
{
    public class Scan
    {
        private readonly ILogger _log;
        private readonly string _scanRoot;

        /// <summary>
        /// Minimal state in this class!
        /// </summary>
        /// <param name="scanRoot"></param>
        /// <param name="logger">Pass in a special logger, such as a null logger for unit testing. Else use the standard log for this class</param>
        public Scan(string scanRoot, ILogger logger = null)
        {
            _scanRoot = scanRoot;
            if (logger == null)
            {
                _log = new LoggerBruDupeFileRemover(typeof(Scan));
            }
        }

        /// <summary>
        /// For each list of potential duplicates (which have same Directory+BaseName_Extension)
        ///     Check the file contents of each file in the list to be sure they are equal.
        ///     Pull out those with unequal file contents
        ///     Return the list if it is still > 1
        /// </summary>
        /// <param name="potentialDuplicatesLists"></param>
        /// <param name="token"></param>
        /// <param name="onProgressActualDuplicates">report every file checked by reporting its size</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public IReadOnlyCollection<Duplicate> GetActualDuplicatesParallel(IEnumerable<IEnumerable<Duplicate>> potentialDuplicatesLists, CancellationToken token,
            IProgress<ReportActualDuplicates> onProgressActualDuplicates)
        {
            var duplicatesBag = new ConcurrentBag<Duplicate>();
            var exceptions = new ConcurrentQueue<Exception>();
            Parallel.ForEach(potentialDuplicatesLists, (potentialDuplicates, loopState) =>
            {
                try
                {
                    if (token.IsCancellationRequested)
                    {
                        loopState.Stop();
                        token.ThrowIfCancellationRequested();
                    }
                    var enumerable = potentialDuplicates as IList<Duplicate> ?? potentialDuplicates.ToList();
                    var numPotentialDuplicateFilesProcessed = enumerable.Count;
                    var numPotentialDuplicateFileBytesProcessed = enumerable.First().Size * numPotentialDuplicateFilesProcessed;
                    var duplicates = RemoveWhereFileContentsNotEqual(enumerable);
                    var numActualDuplicateFilesFound = duplicates.Count == 0 ? 0 : duplicates.Count - 1; // we don't count DupeNumber == 0
                    var reportData = new ReportActualDuplicates(numActualDuplicateFilesFound, numPotentialDuplicateFilesProcessed, numPotentialDuplicateFileBytesProcessed);
                    onProgressActualDuplicates.Report(reportData);
                    var dupeNumber = 0;
                    foreach (var duplicate in duplicates)
                    {
                        duplicate.DupeNumber = dupeNumber++;
                        duplicatesBag.Add(duplicate);
                    }
                }
                catch (Exception ex)
                {
                    _log.WithCallInfo().Error(ex.Message, ex);
                    exceptions.Enqueue(ex);
                }
            });
            if (exceptions.Count > 0)
            {
                throw new AggregateException(exceptions);
            }
            return duplicatesBag;
        }

        /// <summary>
        /// Recursive routine to get the list of subdirectories below subDirs
        /// </summary>
        /// <param name="subDirs"></param>
        /// <param name="token"></param>
        /// <param name="onProgressNumDirectoriesFound"></param>
        /// <returns></returns>
        public IReadOnlyCollection<string> GetDirectoriesParallel(string[] subDirs, CancellationToken token, IProgress<int> onProgressNumDirectoriesFound)
        {
            var sw = Stopwatch.StartNew();
            var bag = new ConcurrentBag<string[]> { subDirs };
            GetDirectoriesParallel(subDirs, bag, token, onProgressNumDirectoriesFound);
            var result = bag.SelectMany(x => x).ToList();
            _log.WithCallInfo().Debug("GetDirectoriesParallel() for {0} took {1} msecs to get {2} directories", _scanRoot, sw.ElapsedMilliseconds, result.Count);
            return result;
        }

        /// <summary>
        /// Get a list of list of duplicates from the array of fileInfos, all of which must be for a given directory.
        /// Each list has the same BaseNameWithExtension, so they are probably duplicates, but we haven't yet checked that their contents are equal
        /// </summary>
        /// <param name="fileInfos"></param>
        /// <param name="extraCopyStrings"></param>
        /// <param name="aggressive"></param>
        /// <returns></returns>
        public IEnumerable<IEnumerable<Duplicate>> GetPotentialDuplicates(FileInfo[] fileInfos, ICollection<string> extraCopyStrings, bool aggressive)
        {
            var result = new List<IEnumerable<Duplicate>>();

            // First get groups by length where multiple files have the same Length
            var groupedByLength = fileInfos
                .GroupBy(f => f.Length)
                .Where(grp => grp.Count() > 1);
            foreach (var groupSameLength in groupedByLength)
            {
                // Split these same-length files' names into components for comparison
                var maybesSameLength = new List<Duplicate>();
                foreach (var fi in groupSameLength)
                {
                    var maybe = new Duplicate(fi, extraCopyStrings, aggressive);
                    maybesSameLength.Add(maybe);
                }

                // Now get groups by extension where multiple files have the same BaseName and Extension and Bytes
                var groupedByBaseNameExtension = maybesSameLength
                    .GroupBy(x => x.BaseNameWithExtension)
                    .Where(grp => grp.Count() > 1);
                foreach (var groupSameBaseNameExtension in groupedByBaseNameExtension)
                {
                    var duplicates = groupSameBaseNameExtension.Select(group => group);
                    result.Add(duplicates);
                }
            }
            return result;
        }

        /// <summary>
        /// For each directory in directories, check the files for duplicates (based on names but not file contents).
        /// Add each set of potential duplicates and return all the sets
        /// </summary>
        /// <param name="directories"></param>
        /// <param name="extraCopyStrings"></param>
        /// <param name="aggressive"></param>
        /// <param name="token"></param>
        /// <param name="onProgressPotentialDuplicatesFound"></param>
        /// <param name="onProgressFilesFound"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public IEnumerable<IEnumerable<Duplicate>> GetPotentialDuplicatesParallel(
            IReadOnlyCollection<string> directories,
            ICollection<string> extraCopyStrings, bool aggressive, CancellationToken token,
            IProgress<int> onProgressPotentialDuplicatesFound, IProgress<int> onProgressFilesFound)
        {
            var exceptions = new ConcurrentQueue<Exception>();
            var duplicatesBag = new ConcurrentBag<IEnumerable<Duplicate>>();
            Parallel.ForEach(directories, (directory, loopState) =>
            {
                try
                {
                    if (token.IsCancellationRequested)
                    {
                        loopState.Stop();
                        token.ThrowIfCancellationRequested();
                    }
                    var di = new DirectoryInfo(directory);
                    var fileInfos = di.GetFiles();
                    onProgressFilesFound.Report(fileInfos.Length);
                    if (fileInfos.Length <= 0)
                    {
                        return;
                    }
                    var dupeLists = GetPotentialDuplicates(fileInfos, extraCopyStrings, aggressive).ToList();
                    if (dupeLists.Count <= 0)
                    {
                        return;
                    }
                    foreach (var list in dupeLists)
                    {
                        duplicatesBag.Add(list);
                    }
                    var numPotentialDuplicateFilesFound = dupeLists.SelectMany(x =>
                    {
                        var duplicates = x as IList<Duplicate> ?? x.ToList();
                        return duplicates;
                    }).Count() - dupeLists.Count;
                    onProgressPotentialDuplicatesFound.Report(numPotentialDuplicateFilesFound);
                }
                catch (UnauthorizedAccessException e)
                {
                    _log.WithCallInfo().Debug(e.Message);
                }
                catch (DirectoryNotFoundException e)
                {
                    _log.WithCallInfo().Debug(e.Message);
                }
                catch (IOException e)
                {
                    _log.WithCallInfo().Debug(e.Message);
                }
                catch (Exception ex)
                {
                    _log.WithCallInfo().Error(ex.Message, ex);
                    exceptions.Enqueue(ex);
                }
            });
            if (exceptions.Count > 0)
            {
                throw new AggregateException(exceptions);
            }
            return duplicatesBag;
        }

        /// <summary>
        /// Remove from duplicates any file whose contents are not equal to the first one.
        /// </summary>
        /// <param name="duplicates">2 or more filenames with the same Directory+BaseName+Extension</param>
        /// <returns>2 or more duplicates with the same file contents, or an empty collection</returns>
        public IReadOnlyCollection<Duplicate> RemoveWhereFileContentsNotEqual(IEnumerable<Duplicate> duplicates)
        {
            const int bufferSize = 65536;
            var buffer0 = new byte[bufferSize];
            var buffer = new byte[bufferSize];
            var result = duplicates
                .OrderBy(d => d.CopySuffixes)
                .ToList();
            var streams = result.Select(d => d.FileInfo.OpenRead()).ToList();
            var stream0 = streams[0];
            var numBytesStillToRead = stream0.Length; // all files are the same length
            while (numBytesStillToRead > 0)
            {
                var numBytesReadToBuffer = stream0.Read(buffer0, 0, bufferSize);
                if (numBytesReadToBuffer == 0)
                {
                    // EOF reached
                    break;
                }
                numBytesStillToRead -= numBytesReadToBuffer;

                // Convert bytes to Int64s for the base stream
                var stream0Longs = new long[bufferSize / 8];
                var numLongs = numBytesReadToBuffer / 8;
                for (int i = 0; i < numLongs; i++)
                {
                    var n = BitConverter.ToInt64(buffer0, i * 8);
                    stream0Longs[i] = n;
                }

                // Now compare each other stream to stream0
                for (int j = 1; j < result.Count; j++)
                {
                    var numJ = streams[j].Read(buffer, 0, bufferSize);
                    if (numJ != numBytesReadToBuffer)
                    {
                        throw new BruDupeFileRemoverException($"Unexpectedly read {numJ} when expected {numBytesReadToBuffer} bytes from {result[j]}");
                    }
                    for (int i = 0; i < numBytesReadToBuffer / 8; i++)
                    {
                        var n = BitConverter.ToInt64(buffer, i * 8);
                        if (n != stream0Longs[i])
                        {
                            // We have a mismatch
                            _log.WithCallInfo().Info("File contents mismatch between {0} and {1}", result[j], result[0]);
                            result.RemoveAt(j);
                            streams[j].Dispose();
                            streams.RemoveAt(j);
                            j--;
                            break;
                        }
                    }
                }
                if (numLongs * 8 < numBytesReadToBuffer)
                {
                    // We have some leftover bytes to compare
                    var pos = numLongs * 8;
                    while (result.Count > 1 && pos < numBytesReadToBuffer)
                    {
                        var byte0 = buffer0[pos];
                        for (int j = 1; j < result.Count; j++)
                        {
                            var byteJ = buffer[pos];
                            if (byteJ != byte0)
                            {
                                // We have a mismatch
                                _log.WithCallInfo().Info("File contents mismatch between {0} and {1}", result[j], result[0]);
                                result.RemoveAt(j);
                                streams[j].Dispose();
                                streams.RemoveAt(j);
                                break;
                            }
                        }
                        pos++;
                    }
                }
            }
            streams.ForEach(x => x.Dispose());
            if (result.Count < 2)
            {
                // None of the duplicates matched, so empty this list
                result.Clear();
            }
            return result;
        }

        /// <summary>
        /// Recursive routine to get the list of subdirectories below subDirs.
        /// For a huge directory this took me 1.2 seconds compared to 6.7 seconds using Directory.GetDirectories() or Directory.EnumerateDirectories.AsParallel()
        /// https://msdn.microsoft.com/en-us/library/ff477033(v=vs.110).aspx
        /// </summary>
        /// <param name="subDirs"></param>
        /// <param name="result"></param>
        /// <param name="token"></param>
        /// <param name="onProgressNumDirectoriesFound"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void GetDirectoriesParallel(string[] subDirs, ConcurrentBag<string[]> result, CancellationToken token, IProgress<int> onProgressNumDirectoriesFound)
        {
            // Execute in parallel if there are enough subdirs
            // Otherwise, execute sequentially.
            var exceptions = new ConcurrentQueue<Exception>();

            // Determine whether to parallelize based on processor count.
            if (subDirs.Length < Environment.ProcessorCount)
            {
                foreach (var subDir in subDirs)
                {
                    try
                    {
                        token.ThrowIfCancellationRequested();
                        var subSubDirs = Directory.GetDirectories(subDir);
                        if (subSubDirs.Length > 0)
                        {
                            onProgressNumDirectoriesFound.Report(subSubDirs.Length);
                            result.Add(subSubDirs);
                            GetDirectoriesParallel(subSubDirs, result, token, onProgressNumDirectoriesFound);
                        }
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        // Thrown if we do not have discovery permission on the directory.
                        _log.WithCallInfo().Debug(e.Message);
                        return;
                    }
                    catch (DirectoryNotFoundException e)
                    {
                        // Thrown if another process has deleted the directory after we retrieved its name.
                        _log.WithCallInfo().Debug(e.Message);
                        return;
                    }
                    catch (Exception ex)
                    {
                        _log.WithCallInfo().Error(ex.Message, ex);
                        exceptions.Enqueue(ex);
                    }
                }
            }
            else
            {
                Parallel.ForEach(subDirs, (subDir, loopState) =>
                {
                    try
                    {
                        if (token.IsCancellationRequested)
                        {
                            loopState.Stop();
                            token.ThrowIfCancellationRequested();
                        }
                        var subSubDirs = Directory.GetDirectories(subDir);
                        if (subSubDirs.Length > 0)
                        {
                            onProgressNumDirectoriesFound.Report(subSubDirs.Length);
                            result.Add(subSubDirs);
                            GetDirectoriesParallel(subSubDirs, result, token, onProgressNumDirectoriesFound);
                        }
                    }
                    catch (UnauthorizedAccessException e)
                    {
                            // Thrown if we do not have discovery permission on the directory.
                            _log.WithCallInfo().Debug(e.Message);
                    }
                    catch (DirectoryNotFoundException e)
                    {
                            // Thrown if another process has deleted the directory after we retrieved its name.
                            _log.WithCallInfo().Debug(e.Message);
                    }
                    catch (Exception ex)
                    {
                        _log.WithCallInfo().Error(ex.Message, ex);
                        exceptions.Enqueue(ex);
                    }
                });
            }
            if (exceptions.Count > 0)
            {
                throw new AggregateException(exceptions);
            }
        }
    }
}