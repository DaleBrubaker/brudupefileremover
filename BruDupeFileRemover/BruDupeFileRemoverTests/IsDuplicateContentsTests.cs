﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
#pragma warning disable 1822

using System.Collections.Generic;
using System.IO;
using System.Text;
using BruDupeFileRemover;
using BruDupeFileRemover.Logging;
using NUnit.Framework;

namespace BruDupeFileRemoverTests
{
    [TestFixture]
    public class IsDuplicateContentsTests
    {
        private Scan _scan;

        [SetUp]
        public void Setup()
        {
            _scan = new Scan(null, new NullLogger());
            var tempFolder = Path.GetTempPath();
            Directory.SetCurrentDirectory(tempFolder);
        }

        [Test]
        public void ContentsNull()
        {
            var fi1 = new FileInfo("file1");
            var fi2 = new FileInfo("file2");
            File.WriteAllBytes(fi1.FullName, new byte[0]);
            File.WriteAllBytes(fi2.FullName, new byte[0]);
            var dupe1 = new Duplicate(fi1);
            var dupe2 = new Duplicate(fi2);
            var dupes = new List<Duplicate> { dupe1, dupe2 };
            var result = _scan.RemoveWhereFileContentsNotEqual(dupes);
            fi1.Delete();
            fi2.Delete();
            Assert.AreEqual(result.Count, 2);
        }

        [Test]
        public void ContentsEqual()
        {
            var fi1 = new FileInfo("file1");
            var fi2 = new FileInfo("file2");
            File.WriteAllText(fi1.FullName, @"This is same text");
            File.WriteAllText(fi2.FullName, @"This is same text");
            var dupe1 = new Duplicate(fi1);
            var dupe2 = new Duplicate(fi2);
            var dupes = new List<Duplicate> { dupe1, dupe2 };
            var result = _scan.RemoveWhereFileContentsNotEqual(dupes);
            fi1.Delete();
            fi2.Delete();
            Assert.AreEqual(result.Count, 2);
        }

        [Test]
        public void ContentsDifferAfterLongs()
        {
            var fi1 = new FileInfo("file1");
            var fi2 = new FileInfo("file2");
            File.WriteAllText(fi1.FullName, @"This is some text1");
            File.WriteAllText(fi2.FullName, @"This is some text2");
            var dupe1 = new Duplicate(fi1);
            var dupe2 = new Duplicate(fi2);
            var dupes = new List<Duplicate> { dupe1, dupe2 };
            var result = _scan.RemoveWhereFileContentsNotEqual(dupes);
            fi1.Delete();
            fi2.Delete();
            Assert.IsEmpty(result);
        }

        [Test]
        public void ContentsDifferWithinLongs()
        {
            var fi1 = new FileInfo("file1");
            var fi2 = new FileInfo("file2");
            File.WriteAllText(fi1.FullName, @"This is some text");
            File.WriteAllText(fi2.FullName, @"This is different");
            var dupe1 = new Duplicate(fi1);
            var dupe2 = new Duplicate(fi2);
            var dupes = new List<Duplicate> { dupe1, dupe2 };
            var result = _scan.RemoveWhereFileContentsNotEqual(dupes);
            fi1.Delete();
            fi2.Delete();
            Assert.IsEmpty(result);
        }

        [Test]
        public void ContentsDifferAt100000()
        {
            var fi1 = new FileInfo("file1");
            var fi2 = new FileInfo("file2");
            var sb = new StringBuilder(100000);
            for (int i = 0; i < 100000; i++)
            {
                sb.Append('x');
            }
            File.WriteAllText(fi1.FullName, sb.ToString());
            sb[99999] = 'y';
            File.WriteAllText(fi2.FullName, sb.ToString());
            var dupe1 = new Duplicate(fi1);
            var dupe2 = new Duplicate(fi2);
            var dupes = new List<Duplicate> { dupe1, dupe2 };
            var result = _scan.RemoveWhereFileContentsNotEqual(dupes);
            fi1.Delete();
            fi2.Delete();
            Assert.IsEmpty(result);
        }

        [Test]
        public void ContentsDifferSecondOfThree()
        {
            var fi1 = new FileInfo("file1");
            var fi2 = new FileInfo("file2");
            var fi3 = new FileInfo("file3");
            File.WriteAllText(fi1.FullName, @"This is some text");
            File.WriteAllText(fi2.FullName, @"This is different");
            File.WriteAllText(fi3.FullName, @"This is some text");
            var dupe1 = new Duplicate(fi1);
            var dupe2 = new Duplicate(fi2);
            var dupe3 = new Duplicate(fi3);
            var dupes = new List<Duplicate> { dupe1, dupe2, dupe3 };
            var result = _scan.RemoveWhereFileContentsNotEqual(dupes);
            fi1.Delete();
            fi2.Delete();
            fi3.Delete();
            Assert.AreEqual(result.Count, 2);
        }

    }
}