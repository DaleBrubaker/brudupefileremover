﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
#pragma warning disable 1822,CC091
#pragma warning disable CC0091 // Use static method
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BruDupeFileRemover;
using BruDupeFileRemover.Logging;
using NUnit.Framework;

namespace BruDupeFileRemoverTests
{
    [TestFixture]
    public class FileOperationsTests
    {

        private Scan _scan;
        private string _tempFolder;

        [SetUp]
        public void Setup()
        {
            _scan = new Scan(null, new NullLogger());
            _tempFolder = Path.GetTempPath();
            Directory.SetCurrentDirectory(_tempFolder);
        }

        private FileInfo[] BuildFileLinfos(params string[] args)
        {
            var result = new FileInfo[args.Length];
            for (int i = 0; i < args.Length; i++)
            {
                var fi = new FileInfo(args[i]);
                File.WriteAllBytes(fi.FullName, new byte[0]);
                result[i] = fi;
            }
            return result;
        }

        private void Delete(FileInfo [] fileInfos)
        {
            foreach (var fi in fileInfos)
            {
                fi.Delete();
            }
        }

        [Test]
        public void SingleFileLength()
        {
            var fileInfos = BuildFileLinfos("File.txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameExtensionsDiffer()
        {
            var fileInfos = BuildFileLinfos("Test.txt", "Test.tt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameExtensionsFirstNull()
        {
            var fileInfos = BuildFileLinfos("Test", "Test.txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameExtensionsNull()
        {
            var fileInfos = BuildFileLinfos("Test", "Test");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameLeadingZeroes()
        {
            var fileInfos = BuildFileLinfos("Test (001).txt", "Test (002).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameNonDigitBetweenParens()
        {
            var fileInfos = BuildFileLinfos("Test (0x1).txt", "Test (2).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameNonDigitSpaceBetweenParens()
        {
            var fileInfos = BuildFileLinfos("Test ( 01).txt", "Test (2).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameNoParenWithNoParen()
        {
            var fileInfos = BuildFileLinfos("Test_2a-1.txt", "Test_2a-2.txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameNoParenWithParen()
        {
            var fileInfos = BuildFileLinfos("Test.txt", "Test (1).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameNoSpaceBeforeLeftParen()
        {
            var fileInfos = BuildFileLinfos("Test(1).txt", "Test(2).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.IsEmpty(duplicates);
            Delete(fileInfos);
        }

        [Test]
        public void NameNull()
        {
            var fileInfos = BuildFileLinfos(".Test", ".Test");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameNullExtension()
        {
            var fileInfos = BuildFileLinfos("Test", "Test (1)");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameParenWithNoParen()
        {
            var fileInfos = BuildFileLinfos("Test.txt", "Test (1).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameParenWithParen()
        {
            var fileInfos = BuildFileLinfos("Test (1).txt", "Test (2).txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }


        [Test]
        public void NameParenWithParenCopy()
        {
            var fileInfos = BuildFileLinfos("Test (1).txt", "Test (2) - Copy.txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameNoParenWithNoParenCopy()
        {
            var fileInfos = BuildFileLinfos("Test.txt", "Test - Copy.txt");
            var duplicateLists = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();
            var duplicates = duplicateLists.SelectMany(x =>
            {
                var enumerable = x as IList<Duplicate> ?? x.ToList();
                return enumerable;
            }).ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Delete(fileInfos);
        }

        [Test]
        public void NameSpecial1()
        {
            var fileInfos = BuildFileLinfos("Shane40thBDayS 337 (1).JPG", "Shane40thBDayS 337 (2) - Copy.JPG");
            var duplicatesList = _scan.GetPotentialDuplicates(fileInfos, null, false).ToList();

            Assert.AreEqual(duplicatesList.Count,1);
            var duplicates = duplicatesList[0].ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Assert.AreEqual(duplicates[0].BaseName, "Shane40thBDayS 337");
            Delete(fileInfos);
        }

        [Test]
        public void NameSpecial2()
        {
            var fileInfos = BuildFileLinfos("201212091900.Last.ntd-DaleB-Laptop.gz", "201212091900.Last.ntd-XPS8700.gz");
            var extraCopyStrings = new List<string> { "-DaleB-Laptop", "-XPS8700" };
            var duplicatesList = _scan.GetPotentialDuplicates(fileInfos, extraCopyStrings, true).ToList();

            Assert.AreEqual(duplicatesList.Count, 1);
            var duplicates = duplicatesList[0].ToList();
            Assert.AreEqual(duplicates.Count, 2);
            Assert.AreEqual(duplicates[0].BaseName, "201212091900.Last.ntd");
            Delete(fileInfos);
        }

    }
}