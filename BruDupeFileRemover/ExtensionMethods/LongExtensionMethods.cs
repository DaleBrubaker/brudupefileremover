﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;

namespace BruDupeFileRemover.ExtensionMethods
{
    public static class LongExtensionMethods
    {
        public static string ToBytesPrettyFormat(this long numBytes)
        {
            var kb = numBytes / 1024.0;
            if (numBytes == 0)
            {
                return "0 KB";
            }
            if (Math.Abs(kb) < 1024.0)
            {
                return $"{kb:n1} KB";
            }
            var mb = numBytes / 1024.0 / 1024.0;
            if (Math.Abs(mb) < 1024.0)
            {
                return $"{mb:n1} MB";
            }
            var gb = numBytes / 1024.0 / 1024.0 / 1024.0;
            return $"{gb:n1} GB";
        }
    }
}
