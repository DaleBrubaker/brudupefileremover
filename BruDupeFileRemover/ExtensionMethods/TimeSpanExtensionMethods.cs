﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Text;

namespace BruDupeFileRemover.ExtensionMethods
{
    public static class TimeSpanExtensionMethods
    {
        /// <summary>
        /// From http://stackoverflow.com/questions/5438363/timespan-pretty-time-format-in-c-sharp
        /// </summary>
        /// <param name="span"></param>
        /// <returns></returns>
        public static string ToPrettyFormat(this TimeSpan span)
        {
            if (span == TimeSpan.Zero)
            {
                return "0 minutes";
            }
            var sb = new StringBuilder();
            if (span.Days > 0)
            {
                sb.AppendFormat("{0} day{1} ", span.Days, span.Days > 1 ? "s" : "");
            }
            if (span.Hours > 0)
            {
                sb.AppendFormat("{0} hour{1} ", span.Hours, span.Hours > 1 ? "s" : "");
            }
            if (span.Minutes > 0)
            {
                sb.AppendFormat("{0} minute{1} ", span.Minutes, span.Minutes > 1 ? "s" : "");
            }
            if (span.Seconds > 0)
            {
                sb.AppendFormat("{0} second{1} ", span.Seconds, span.Seconds > 1 ? "s" : "");
            }
            if (sb.Length == 0 && span.Milliseconds > 0)
            {
                sb.AppendFormat("{0} millisecond{1} ", span.Milliseconds, span.Milliseconds > 1 ? "s" : "");
            }
            return sb.ToString();
        }
    }
}