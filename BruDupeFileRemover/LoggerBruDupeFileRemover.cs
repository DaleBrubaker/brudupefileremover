﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover
{
    internal class LoggerBruDupeFileRemover : ILogger
    {
        private readonly ILogger _logger;

        public LoggerBruDupeFileRemover(string className)
        {
            ClassName = className;
            _logger = new Log4NetLogger(ClassName, "log4net.config");
        }

        public LoggerBruDupeFileRemover(Type type)
        {
            ClassName = type.Name;
            _logger = new Log4NetLogger(type, "log4net.config");
        }

        public string ClassName { get; }

        public void Log(LogEntry entry)
        {
            _logger.Log(entry);
        }

        public void LogFormat(LogEntryFormat entry)
        {
            _logger.LogFormat(entry);
        }

        public override string ToString()
        {
            return $"Log4Net logger for {ClassName}";
        }
    }
}