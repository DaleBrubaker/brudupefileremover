﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Threading;

namespace BruDupeFileRemover.Config
{
    /// <summary>
    /// Helper class for serializing access to the config file when multiple instances are running.
    /// </summary>
    public sealed class MutexProtector : IDisposable
    {
        private readonly Mutex _mutex;

        public MutexProtector(string name)
        {
            bool createdNew;
            _mutex = new Mutex(true, name, out createdNew);
            if (!createdNew)
            {
                try
                {
                    _mutex.WaitOne();
                }
#pragma warning disable CC0004 // Catch block cannot be empty
                catch (AbandonedMutexException)
                {
                    // ignore
                }
#pragma warning restore CC0004 // Catch block cannot be empty
            }
        }

        public void Dispose()
        {
            _mutex.ReleaseMutex();
            _mutex.Close(); // also does Dispose()
        }
    }
}
