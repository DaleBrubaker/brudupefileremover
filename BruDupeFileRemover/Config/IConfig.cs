﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BruDupeFileRemover.Config
{
    public interface IConfig
    {
        string Directory { get; set; }

        /// <summary>
        /// Load a group.
        /// Files are saved to the directory passed in by the constructor, with names group.xml
        /// If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        void Load(string group);

        /// <summary>
        /// Load a group from a particular fileName instead of from the directory set in the constructor.
        /// Files are loaded from the directory passed in by the constructor, with names group.xml
        /// If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="fileName">The fully-qualified fileName</param>
        void Load(string group, string fileName);

        /// <summary>
        /// Save all groups that have been loaded.
        /// </summary>
        void SaveAll();

        /// <summary>
        /// Save the settings for group to the directory passed in via the constructor
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        void Save(string group);

        /// <summary>
        /// Save the settings for group to fileName
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="fileName">The fully-qualified fileName</param>
        void Save(string group, string fileName);

        /// <summary>
        /// Get the setting for group + id, or null if not previously set
        /// If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <returns></returns>
        XElement GetConfig(string group, string id);

        /// <summary>
        /// Set the config, where the element XName is the id.
        /// If you fail to Load() before doing a SetConfig(), the first SetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="element"></param>
        void SetConfig(string group, XElement element);

        /// <summary>
        /// Get the config as type T. Use for simple types that do have a TypeConverter.
        /// Standard type converters include:
        ///     bool, enum, Font, Color, DateTimeOffset, all number types,
        ///     See http://msdn.microsoft.com/en-us/library/system.componentmodel.typeconverter_derivedtypelist(VS.85).aspx
        /// If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValue">The default value returned if s is null</param>
        /// <returns>The type returned after conversion from s</returns>
        /// <exception cref="System.NotSupportedException">Thrown when no TypeConverter exists for T</exception>
        T GetConfig<T>(string group, string id, T defaultValue);

        /// <summary>
        /// Set a configuration to value.
        /// If you fail to Load() before doing a SetConfig(), the first SetConfig will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        void SetConfig<T>(string group, string id, T value);

        /// <summary>
        /// Get an IEnumerable collection of type T
        /// If you fail to Load() before doing a GetConfigEnumerable(), the first GetConfigEnumerable will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValues"></param>
        /// <returns>IEnumerable collection of type T</returns>
        IEnumerable<T> GetConfigEnumerable<T>(string group, string id, IEnumerable<T> defaultValues);

        /// <summary>
        /// Set configuration for an IEnumerable collection of type T
        /// If you fail to Load() before doing a SetConfigEnumerable(), the first SetConfigEnumerable will start a empty file.
        /// </summary>
        /// <typeparam name="T">The type of each item in the IEnumerable</typeparam>
        /// <typeparam name="TU">The IEnumerable of T/></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="values"></param>
        void SetConfigEnumerable<T, TU>(string group, string id, TU values)
                    where TU : IEnumerable<T>;

        /// <summary>
        /// Get configuration for a type which is IXmlSerializable
        /// If you fail to Load() before doing a GetConfigIXmlSerializable(), the first GetConfigIXmlSerializable will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        T GetConfigIXmlSerializable<T>(string group, string id, T defaultValue)
                    where T : IXmlSerializable, new();

        /// <summary>
        /// Set configuration for a type which is IXmlSerializable
        /// If you fail to Load() before doing a SetConfigIXmlSerializable(), the first SetConfigIXmlSerializable will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        void SetConfigIXmlSerializable<T>(string group, string id, T value)
                    where T : IXmlSerializable;

        /// <summary>
        /// Get configuration for a type to XmlSerialize
        /// If you fail to Load() before doing a GetConfigXmlSerializer(), the first GetConfigXmlSerializer will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        T GetConfigXmlSerializer<T>(string group, string id, T defaultValue);

        /// <summary>
        /// Set configuration for a type to XmlSerialize
        /// If you fail to Load() before doing a SetConfigXmlSerializer(), the first SetConfigXmlSerializer will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        void SetConfigXmlSerializer<T>(string group, string id, T value);

        /// <summary>
        /// Restore the window pointed to by handle to the state previously saved by SaveWindow().
        /// Or if never previously saved, do nothing.
        /// Note: For form, use the Control.Handle property to get the handle.
        /// Under the covers, this uses User32.GetWindowPlacement() which handles multiple screens, edge cases, etc.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="handle"></param>
        void RestoreWindow(string group, string id, IntPtr handle);

        /// <summary>
        /// Save the current state of the window pointed to by handle.
        /// Note: For form, use the Control.Handle property to get the handle.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="handle"></param>
        void SaveWindow(string group, string id, IntPtr handle);

        /// <summary>
        /// Register a type converter to be used to convert between string and type.
        /// Future conversions to and from type will use the registered converter.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="typeConverter"></param>
        void RegisterTypeConverter(Type type, TypeConverter typeConverter);
    }
}
