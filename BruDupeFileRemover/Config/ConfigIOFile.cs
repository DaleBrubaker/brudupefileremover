﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.IO;
using System.Xml.Linq;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover.Config
{
    public class ConfigIOFile : IConfigIO
    {
        private const string ConfigIOFileMutex = "79FB1301-1E45-494B-9B39-5A6ACA88B84E";
        private readonly ILogger _logger;

        public ConfigIOFile(ILogger logger)
        {
            _logger = logger;
        }

        
        /// <summary>
        /// Return an XElement returned from fileName if possible.
        /// If the file doesn't exist or there is an error, return an empty XElement named group
        /// </summary>
        /// <param name="fileName">The fully-qualified fileName</param>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <returns></returns>
        public XElement Load(string fileName, string group)
        {
            using (new MutexProtector(ConfigIOFileMutex))
            {
                try
                {
                    var settings = File.Exists(fileName) ? XElement.Load(fileName) : new XElement(group);
                    return settings;
                }
                catch (Exception ex)
                {
                    _logger.WithCallInfo().Error(ex.ToString());
                    throw;
                }
            }
        }

        /// <summary>
        /// Save settings to fileName
        /// </summary>
        /// <param name="fileName">The fully-qualified fileName</param>
        /// <param name="settings">An XElement</param>
        public void Save(string fileName, XElement settings)
        {
            var directory = Path.GetDirectoryName(fileName);
            if (!string.IsNullOrEmpty(directory) && !Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            using (new MutexProtector(ConfigIOFileMutex))
            {
                try
                {
                    settings.Save(fileName);
                }
                catch (Exception ex)
                {
                    _logger.WithCallInfo().Error(ex.ToString());
                    throw;
                }
            }
        }
   }
}