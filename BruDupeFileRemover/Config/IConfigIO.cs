﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//

using System.Xml.Linq;

namespace BruDupeFileRemover.Config
{
    /// <summary>
    /// The actual I/O for saving and getting settings
    /// </summary>
    public interface IConfigIO
    {
        /// <summary>
        /// Return an XElement returned from fileName if possible.
        /// If the file doesn't exist or there is an error, return an empty XElement named group
        /// </summary>
        /// <param name="fileName">The fully-qualified fileName</param>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <returns></returns>
        XElement Load(string fileName, string group);

        /// <summary>
        /// Save settings to fileName
        /// </summary>
        /// <param name="fileName">The fully-qualified fileName</param>
        /// <param name="settings">An XElement</param>
        void Save(string fileName, XElement settings);
    }
}
