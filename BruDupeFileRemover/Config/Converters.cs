﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using BruDupeFileRemover.Exceptions;

namespace BruDupeFileRemover.Config
{
    /// <summary>
    /// Useful converters, from ILSpy SessionSettings.cs
    /// Throws NotSupportedException if no TypeConverter between T and String.
    /// </summary>
    public static class Converters
    {
        private static readonly Dictionary<Type, TypeConverter> RegisteredConverters = new Dictionary<Type, TypeConverter>();

        public static void SetRegisteredConverter(Type type, TypeConverter typeConverter)
        {
            RegisteredConverters[type] = typeConverter;
        }

        /// <summary>
        /// Get a value from a string
        /// </summary>
        /// <typeparam name="T">The type of the defaultValue and the type returned</typeparam>
        /// <param name="s">The input string</param>
        /// <param name="defaultValue">The default value returned if s is null</param>
        /// <returns>The type returned after conversion from s</returns>
        /// <exception cref="System.NotSupportedException">Thrown when no TypeConverter exists for T</exception>
        /// <exception cref="T:System.BruDupeFileRemoverException"></exception>
        public static T FromString<T>(string s, T defaultValue)
        {
            if (s == null)
            {
                return defaultValue;
            }
            try
            {
                var c = RegisteredConverters.ContainsKey(typeof(T)) ? RegisteredConverters[typeof(T)] : TypeDescriptor.GetConverter(typeof(T));
                if (c == null || !c.CanConvertFrom(typeof(string)))
                {
                    throw new BruDupeFileRemoverException($"Can't convert from string to {typeof(T).FullName}");
                }
                return (T)c.ConvertFromInvariantString(s);
            }
            catch (FormatException)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Get from string
        /// </summary>
        /// <typeparam name="T">The type of the defaultValue and the type returned</typeparam>
        /// <param name="s">The input string</param>
        /// <returns>The type returned after conversion from s</returns>
        /// <exception cref="System.NotSupportedException">Thrown when no TypeConverter exists for T</exception>
        /// <exception cref="T:System.BruDupeFileRemoverException"></exception>
        public static T FromString<T>(string s)
        {
            var c = RegisteredConverters.ContainsKey(typeof(T)) ? RegisteredConverters[typeof(T)] : TypeDescriptor.GetConverter(typeof(T));
            if (c == null || !c.CanConvertFrom(typeof(string)))
            {
                throw new BruDupeFileRemoverException($"Can't convert from string to {typeof(T).FullName}");
            }
            return (T)c.ConvertFromInvariantString(s);
        }

        public static string ToString<T>(T obj)
        {
            var c = RegisteredConverters.ContainsKey(typeof(T)) ? RegisteredConverters[typeof(T)] : TypeDescriptor.GetConverter(typeof(T));
            if (c == null || !c.CanConvertTo(typeof(string)))
            {
                throw new BruDupeFileRemoverException($"Can't convert from {typeof(T).FullName} to string");
            }
            return c.ConvertToInvariantString(obj);
        }
    }
}
