﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//    
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using BruDupeFileRemover.Exceptions;
using BruDupeFileRemover.Logging;

namespace BruDupeFileRemover.Config
{
    public class Config : IConfig
    {
        /// <summary>
        ///     Does the actual Load and Save
        /// </summary>
        private readonly IConfigIO _configIO;

        /// <summary>
        ///     The key is group, the value is the fully-qualified fileName for the file associated with the group.
        /// </summary>
        private readonly Dictionary<string, string> _fileNames = new Dictionary<string, string>();

        private readonly object _fileNamesLock = new object();
        private readonly ILogger _logger;

        /// <summary>
        ///     The key is group. Each group has its own XElement root
        /// </summary>
        private readonly Dictionary<string, XElement> _settings = new Dictionary<string, XElement>();

        private readonly object _settingsLock = new object();

        /// <summary>
        ///     Info to configure the Config is passed into the constructor.
        /// </summary>
        /// <param name="directory">The default directory for Load(group) and Save(group)</param>
        /// <param name="configIO">Does the actual Load and Save</param>
        /// <param name="logger"></param>
        public Config(string directory, IConfigIO configIO, ILogger logger)
            : this(configIO, logger)
        {
            if (directory == null)
            {
                directory = "";
            }
            Directory = directory;
        }

        /// <summary>
        ///     Info to configure the Config is passed into the constructor.
        ///     This constructor is used for MEF, for injection -- requiring us to set the Directory property.
        /// </summary>
        /// <param name="configIO">Does the actual Load and Save</param>
        /// <param name="logger"></param>
        public Config(IConfigIO configIO, ILogger logger)
        {
            _configIO = configIO;
            _logger = logger;
        }

        /// <summary>
        /// Gets or sets the default directory for Load(group) and Save(group)
        /// </summary>
        public string Directory { get; set; }

        /// <summary>
        ///     Get the setting for group + id, or null if not previously set.
        ///     Use this for more complex types that don't have a TypeConverter
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <returns>an XElement</returns>
        public XElement GetConfig(string group, string id)
        {
            var settings = GetSettings(group);
            var result = settings.Element(id);
            return result;
        }

        /// <summary>
        ///     Get the config as type T. Use for simple types that do have a TypeConverter.
        ///     Standard type converters include:
        ///     bool, enum, Font, Color, DateTimeOffset, all number types,
        ///     See http://msdn.microsoft.com/en-us/library/system.componentmodel.typeconverter_derivedtypelist(VS.85).aspx
        ///     If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValue">The default value returned if s is null</param>
        /// <returns>The type returned after conversion from s</returns>
        /// <exception cref="System.NotSupportedException">Thrown when no TypeConverter exists for T</exception>
        public T GetConfig<T>(string group, string id, T defaultValue)
        {
            var settings = GetSettings(group);
            var element = settings.Element(id);
            if (element == null)
            {
                return defaultValue;
            }
            var value = element.Value;
            try
            {
            }
            catch (Exception exception)
            {
                _logger.WithCallInfo().Error("Ignoring exception: {0}", exception.Message);
                throw;
            }
            var result = Converters.FromString(value, defaultValue);
            return result;
        }

        /// <summary>
        ///     Get an IEnumerable collection of type T
        ///     If you fail to Load() before doing a GetConfigEnumerable(), the first GetConfigEnumerable will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValues"></param>
        /// <returns>IEnumerable collection of type T</returns>
        public IEnumerable<T> GetConfigEnumerable<T>(string group, string id, IEnumerable<T> defaultValues)
        {
            var settings = GetSettings(group);
            var element = settings.Element(id);
            if (element == null)
            {
                return new List<T>(defaultValues);
            }

            var result = new List<T>();
            if (!element.HasElements)
            {
                return result;
            }

            if (element.HasElements)
            {
                var descendants = element.Descendants();
                foreach (var descendant in descendants)
                {
                    var str = descendant.Value;
                    var value = Converters.FromString<T>(str);
                    result.Add(value);
                }
            }
            return result;
        }

        /// <summary>
        ///     Get configuration for a type which is IXmlSerializable
        ///     If you fail to Load() before doing a GetConfigIXmlSerializable(), the first GetConfigIXmlSerializable will start a
        ///     empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetConfigIXmlSerializable<T>(string group, string id, T defaultValue)
                    where T : IXmlSerializable, new()
        {
            var settings = GetSettings(group);
            var element = settings.Element(id);
            if (element == null)
            {
                return defaultValue;
            }

            var t = new T();
            using (var reader = element.FirstNode.CreateReader())
            {
                t.ReadXml(reader);
            }
            return t;
        }

        /// <summary>
        ///     Get configuration for a type to XmlSerialize
        ///     If you fail to Load() before doing a GetConfigXmlSerializer(), the first GetConfigXmlSerializer will start a empty
        ///     file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetConfigXmlSerializer<T>(string group, string id, T defaultValue)
        {
            var settings = GetSettings(group);
            var element = settings.Element(id);
            if (element == null)
            {
                return defaultValue;
            }

            var firstNode = element.FirstNode;
            if (firstNode != null)
            {
                var str = firstNode.ToString();
                var xs = new XmlSerializer(typeof(T));
                using (var stringReader = new StringReader(str))
                {
                    var obj = xs.Deserialize(stringReader);
                    return (T)obj;
                }
            }

            throw new BruDupeFileRemoverException("Unexpected missing node");
        }

        /// <summary>
        ///     Load a group.
        ///     This is not required, as lazy loading will occur on first request of of an item from a group.
        ///     If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        public void Load(string group)
        {
            if (string.IsNullOrEmpty(group))
            {
                throw new BruDupeFileRemoverException("group cannot be null or empty");
            }
            var fileName = GetConfigFile(group);
            LoadImpl(group, fileName);
        }

        /// <summary>
        ///     Load a group from a particular fileName instead of from the directory set in the constructor.
        ///     Files are loaded from the directory passed in by the constructor, with names group.xml
        ///     If you fail to Load() before doing a GetConfig(), the first GetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="fileName">The fully-qualified fileName</param>
        public void Load(string group, string fileName)
        {
            if (string.IsNullOrEmpty(group))
            {
                throw new BruDupeFileRemoverException("group cannot be null or empty");
            }
            if (string.IsNullOrEmpty(fileName))
            {
                throw new BruDupeFileRemoverException("fileName cannot be null or empty");
            }
            lock (_fileNamesLock)
            {
                _fileNames[group] = fileName;
            }
            LoadImpl(group, fileName);
        }

        /// <summary>
        ///     Register a type converter to be used to convert between string and type.
        ///     Future conversions to and from type will use the registered converter.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="typeConverter"></param>
        public void RegisterTypeConverter(Type type, TypeConverter typeConverter)
        {
            Converters.SetRegisteredConverter(type, typeConverter);
        }

        /// <summary>
        ///     Restore the window pointed to by handle to the state previously saved by SaveWindow().
        ///     Or if never previously saved, do nothing.
        ///     Note: For form, use the Control.Handle property to get the handle.
        ///     Under the covers, this uses User32.GetWindowPlacement() which handles multiple screens, edge cases, etc.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="handle"></param>
        public void RestoreWindow(string group, string id, IntPtr handle)
        {
            var settings = GetSettings(group);
            var element = settings.Element(id);
            if (element == null)
            {
                // Not previously saved, so do nothing.
                return;
            }
            var value = element.Value;
            var wp = Converters.FromString(value, WindowPlacement.WINDOWPLACEMENT.Default);
            NativeMethods.SetWindowPlacement(handle, ref wp);
        }

        /// <summary>
        ///     Save the settings for group to the directory passed in via the constructor
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        public void Save(string group)
        {
            if (string.IsNullOrEmpty(group))
            {
                throw new BruDupeFileRemoverException("group cannot be null or empty");
            }
            var fileName = GetConfigFile(group);
            SaveImpl(group, fileName);
        }

        /// <summary>
        ///     Save the settings for group to fileName
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="fileName">The fully-qualified fileName</param>
        public void Save(string group, string fileName)
        {
            if (string.IsNullOrEmpty(group))
            {
                throw new BruDupeFileRemoverException("group cannot be null or empty");
            }
            if (string.IsNullOrEmpty(fileName))
            {
                throw new BruDupeFileRemoverException("fileName cannot be null or empty");
            }
            lock (_fileNamesLock)
            {
                _fileNames[group] = fileName;
            }
            SaveImpl(group, fileName);
        }

        /// <summary>
        ///     Save all groups
        ///     Files are saved to the directory passed in by the constructor, with names group.xml
        /// </summary>
        public void SaveAll()
        {
            lock (_settingsLock)
            {
                foreach (var group in _settings.Keys)
                {
                    var fileName = _fileNames[group];
                    SaveImpl(group, fileName);
                }
            }
        }

        /// <summary>
        ///     Save the current state of the window pointed to by handle.
        ///     Note: For form, use the Control.Handle property to get the handle.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="handle"></param>
        public void SaveWindow(string group, string id, IntPtr handle)
        {
            WindowPlacement.WINDOWPLACEMENT wp; // = WindowPlacement.WINDOWPLACEMENT.Default;
            NativeMethods.GetWindowPlacement(handle, out wp);
            SetConfig(group, id, wp);
        }

        /// <summary>
        ///     Set the config, where the element XName is the id.
        ///     Use this for more complex types that don't have a TypeConverter
        ///     If you fail to Load() before doing a SetConfig(), the first SetConfig will start a empty file.
        /// </summary>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="element"></param>
        public void SetConfig(string group, XElement element)
        {
            var settings = GetSettings(group);
            settings.SetElementValue(element.Name, element.Value);
        }

        /// <summary>
        ///     Set a configuration to value.
        ///     If you fail to Load() before doing a SetConfig(), the first SetConfig will start a empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public void SetConfig<T>(string group, string id, T value)
        {
            var settings = GetSettings(group);
            var str = Converters.ToString(value);
            settings.SetElementValue(id, str);
        }

        /// <summary>
        ///     Set configuration for an IEnumerable collection of type T
        ///     If you fail to Load() before doing a SetConfigEnumerable(), the first SetConfigEnumerable will start a empty file.
        /// </summary>
        /// <typeparam name="T">The type of each item in the IEnumerable</typeparam>
        /// <typeparam name="TU">The IEnumerable of T/></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="values"></param>
        public void SetConfigEnumerable<T, TU>(string group, string id, TU values)
                    where TU : IEnumerable<T>
        {
            var settings = GetSettings(group);
            var node = settings.Element(id);
            if (node != null)
            {
                node.Remove();
            }
            settings.Add(new XElement(id));
            node = settings.Element(id);
            var list = new List<T>(values);
            for (var i = 0; i < list.Count; i++)
            {
                var str = Converters.ToString(list[i]);
                var index = $"i{i}";
                node.Add(new XElement(index, str));
            }
        }

        /// <summary>
        ///     Set configuration for a type which is IXmlSerializable
        ///     If you fail to Load() before doing a SetConfigIXmlSerializable(), the first SetConfigIXmlSerializable will start a
        ///     empty file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public void SetConfigIXmlSerializable<T>(string group, string id, T value)
                    where T : IXmlSerializable
        {
            var settings = GetSettings(group);
            var output = new StringBuilder();
            var ws = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                ConformanceLevel = ConformanceLevel.Fragment,
                CheckCharacters = true
            };
            using (var writer = XmlWriter.Create(output, ws))
            {
                value.WriteXml(writer);
            }

            var node = settings.Element(id);
            if (node != null)
            {
                node.Remove();
            }

            settings.Add(new XElement(id, XElement.Parse(output.ToString())));
        }

        /// <summary>
        ///     Set configuration for a type to XmlSerialize
        ///     If you fail to Load() before doing a SetConfigXmlSerializer(), the first SetConfigXmlSerializer will start a empty
        ///     file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">A group name cannot have spaces!</param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public void SetConfigXmlSerializer<T>(string group, string id, T value)
        {
            var settings = GetSettings(group);
            using (var sw = new StringWriter())
            {
                var xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, value);
                var str = sw.ToString();
                var node = settings.Element(id);
                if (node != null)
                {
                    node.Remove();
                }
                settings.Add(new XElement(id, XElement.Parse(str)));
            }
        }

        public override string ToString()
        {
            return $"{Directory}";
        }

        private string GetConfigFile(string group)
        {
            lock (_fileNamesLock)
            {
                if (_fileNames.ContainsKey(group))
                {
                    return _fileNames[group];
                }
                var fileName = Path.Combine(Directory, group + ".xml");
                _fileNames.Add(group, fileName);
                return fileName;
            }
        }

        private XElement GetSettings(string group)
        {
            lock (_settingsLock)
            {
                XElement settings;
                if (!_settings.TryGetValue(group, out settings))
                {
                    settings = new XElement(group);
                    _settings.Add(group, settings);
                }
                return settings;
            }
        }

        private void LoadImpl(string group, string fileName)
        {
            var settings = _configIO.Load(fileName, group);
            lock (_settingsLock)
            {
                if (_settings.ContainsKey(group))
                {
                    _settings[group] = settings;
                }
                else
                {
                    _settings.Add(group, settings);
                }
            }
        }

        private void SaveImpl(string group, string fileName)
        {
            var settings = GetSettings(group);
            _configIO.Save(fileName, settings);
        }
    }
}