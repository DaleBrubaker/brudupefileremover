﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.ComponentModel;
using System.Text;
using BruDupeFileRemover.Exceptions;
using BruDupeFileRemover.ExtensionMethods;

namespace BruDupeFileRemover.Config
{
    /// <summary>
    /// Type converter for a WINDOWPLACEMENT structure
    /// </summary>
    public class WindowPlacementConverter : TypeConverter
    {
        private const char Sep = ',';

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (!(value is string))
            {
                return base.ConvertFrom(context, culture, value);
            }
            var result = WindowPlacement.WINDOWPLACEMENT.Default;
            var str = (string)value;
            var splits = str.Split(Sep);
            result.Length = splits[0].TryParseOrDefaultInt();
            result.Flags = splits[1].TryParseOrDefaultInt();
            if (!Enum.TryParse(splits[2], out result.ShowCmd))
            {
                throw new BruDupeFileRemoverException($"Unable to convert Enum {result.ShowCmd}");
            }
            result.MinPosition.X = splits[3].TryParseOrDefaultInt();
            result.MinPosition.Y = splits[4].TryParseOrDefaultInt();
            result.MaxPosition.X = splits[5].TryParseOrDefaultInt();
            result.MaxPosition.Y = splits[6].TryParseOrDefaultInt();
            result.NormalPosition.Left = splits[7].TryParseOrDefaultInt();
            result.NormalPosition.Top = splits[8].TryParseOrDefaultInt();
            result.NormalPosition.Right = splits[9].TryParseOrDefaultInt();
            result.NormalPosition.Bottom = splits[10].TryParseOrDefaultInt();
            return result;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType != typeof(string))
            {
                return base.ConvertTo(context, culture, value, destinationType);
            }
            var sb = new StringBuilder();
            var wp = (WindowPlacement.WINDOWPLACEMENT)value;
            sb.Append(wp.Length);
            sb.Append(Sep);
            sb.Append(wp.Flags);
            sb.Append(Sep);

            // We always save it hidden, so WPF doesn't flicker a form after EndInit() and before OnLoad()
            wp.ShowCmd = WindowPlacement.ShowWindowCommands.Hide;
            sb.Append(wp.ShowCmd);
            sb.Append(Sep);
            sb.Append(wp.MinPosition.X);
            sb.Append(Sep);
            sb.Append(wp.MinPosition.Y);
            sb.Append(Sep);
            sb.Append(wp.MaxPosition.X);
            sb.Append(Sep);
            sb.Append(wp.MaxPosition.Y);
            sb.Append(Sep);
            sb.Append(wp.NormalPosition.Left);
            sb.Append(Sep);
            sb.Append(wp.NormalPosition.Top);
            sb.Append(Sep);
            sb.Append(wp.NormalPosition.Right);
            sb.Append(Sep);
            sb.Append(wp.NormalPosition.Bottom);
            return sb.ToString();
        }
    }
}
