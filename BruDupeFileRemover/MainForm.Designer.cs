﻿namespace BruDupeFileRemover
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblScanRoot = new System.Windows.Forms.Label();
            this.btnSetScanRoot = new System.Windows.Forms.Button();
            this.btnSetDirectoryMoveRoot = new System.Windows.Forms.Button();
            this.lblMoveRoot = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnScan = new System.Windows.Forms.Button();
            this.lblScan = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnMove = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rtbExtraCopyStrings = new System.Windows.Forms.RichTextBox();
            this.chkBxAggressive = new System.Windows.Forms.CheckBox();
            this.lblMove = new System.Windows.Forms.Label();
            this.duplicateDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.duplicateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.duplicateBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateBindingNavigator)).BeginInit();
            this.duplicateBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblScanRoot
            // 
            this.lblScanRoot.AutoSize = true;
            this.lblScanRoot.Location = new System.Drawing.Point(252, 8);
            this.lblScanRoot.Name = "lblScanRoot";
            this.lblScanRoot.Size = new System.Drawing.Size(65, 13);
            this.lblScanRoot.TabIndex = 0;
            this.lblScanRoot.Text = "lblScanRoot";
            // 
            // btnSetScanRoot
            // 
            this.btnSetScanRoot.Location = new System.Drawing.Point(14, 3);
            this.btnSetScanRoot.Name = "btnSetScanRoot";
            this.btnSetScanRoot.Size = new System.Drawing.Size(137, 23);
            this.btnSetScanRoot.TabIndex = 0;
            this.btnSetScanRoot.Text = "Set Scan Directory";
            this.toolTip1.SetToolTip(this.btnSetScanRoot, "The directory we want to check for duplicate files, including subdirectories.");
            this.btnSetScanRoot.UseVisualStyleBackColor = true;
            this.btnSetScanRoot.Click += new System.EventHandler(this.BtnSetScanRoot_Click);
            // 
            // btnSetDirectoryMoveRoot
            // 
            this.btnSetDirectoryMoveRoot.Location = new System.Drawing.Point(14, 32);
            this.btnSetDirectoryMoveRoot.Name = "btnSetDirectoryMoveRoot";
            this.btnSetDirectoryMoveRoot.Size = new System.Drawing.Size(137, 23);
            this.btnSetDirectoryMoveRoot.TabIndex = 1;
            this.btnSetDirectoryMoveRoot.Text = "Set Move Directory";
            this.toolTip1.SetToolTip(this.btnSetDirectoryMoveRoot, "The directory to which we will move duplicate files.");
            this.btnSetDirectoryMoveRoot.UseVisualStyleBackColor = true;
            this.btnSetDirectoryMoveRoot.Click += new System.EventHandler(this.BtnSetDirectoryMoveRoot_Click);
            // 
            // lblMoveRoot
            // 
            this.lblMoveRoot.AutoSize = true;
            this.lblMoveRoot.Location = new System.Drawing.Point(252, 37);
            this.lblMoveRoot.Name = "lblMoveRoot";
            this.lblMoveRoot.Size = new System.Drawing.Size(67, 13);
            this.lblMoveRoot.TabIndex = 3;
            this.lblMoveRoot.Text = "lblMoveRoot";
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(14, 61);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(137, 23);
            this.btnScan.TabIndex = 2;
            this.btnScan.Text = "Scan";
            this.toolTip1.SetToolTip(this.btnScan, "Find duplicates in the scan directory. No file will be deleted or moved.");
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.BtnScan_ClickAsync);
            // 
            // lblScan
            // 
            this.lblScan.AutoSize = true;
            this.lblScan.Location = new System.Drawing.Point(252, 66);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(42, 13);
            this.lblScan.TabIndex = 6;
            this.lblScan.Text = "lblScan";
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(14, 90);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(137, 23);
            this.btnMove.TabIndex = 3;
            this.btnMove.Text = "Move";
            this.toolTip1.SetToolTip(this.btnMove, "Move the duplicate files in the list from the Scan Directory to the Move Director" +
        "y.");
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.BtnMove_ClickAsync);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Pink;
            this.btnCancel.Location = new System.Drawing.Point(14, 118);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(137, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.toolTip1.SetToolTip(this.btnCancel, "Move the duplicate files in the list from the Scan Directory to the Move Director" +
        "y.");
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rtbExtraCopyStrings);
            this.splitContainer1.Panel1.Controls.Add(this.chkBxAggressive);
            this.splitContainer1.Panel1.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel1.Controls.Add(this.lblMove);
            this.splitContainer1.Panel1.Controls.Add(this.btnSetScanRoot);
            this.splitContainer1.Panel1.Controls.Add(this.btnMove);
            this.splitContainer1.Panel1.Controls.Add(this.lblScanRoot);
            this.splitContainer1.Panel1.Controls.Add(this.lblScan);
            this.splitContainer1.Panel1.Controls.Add(this.btnSetDirectoryMoveRoot);
            this.splitContainer1.Panel1.Controls.Add(this.btnScan);
            this.splitContainer1.Panel1.Controls.Add(this.lblMoveRoot);
            this.splitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.duplicateDataGridView);
            this.splitContainer1.Panel2.Controls.Add(this.duplicateBindingNavigator);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(827, 520);
            this.splitContainer1.SplitterDistance = 144;
            this.splitContainer1.TabIndex = 8;
            // 
            // rtbExtraCopyStrings
            // 
            this.rtbExtraCopyStrings.Location = new System.Drawing.Point(157, 32);
            this.rtbExtraCopyStrings.Name = "rtbExtraCopyStrings";
            this.rtbExtraCopyStrings.Size = new System.Drawing.Size(89, 81);
            this.rtbExtraCopyStrings.TabIndex = 11;
            this.rtbExtraCopyStrings.Text = "-XPS8700\n-DaleB-Laptop";
            // 
            // chkBxAggressive
            // 
            this.chkBxAggressive.AutoSize = true;
            this.chkBxAggressive.Location = new System.Drawing.Point(157, 9);
            this.chkBxAggressive.Name = "chkBxAggressive";
            this.chkBxAggressive.Size = new System.Drawing.Size(78, 17);
            this.chkBxAggressive.TabIndex = 10;
            this.chkBxAggressive.Text = "Aggressive";
            this.chkBxAggressive.UseVisualStyleBackColor = true;
            this.chkBxAggressive.CheckedChanged += new System.EventHandler(this.ChkBxAggressive_CheckedChanged);
            // 
            // lblMove
            // 
            this.lblMove.AutoSize = true;
            this.lblMove.Location = new System.Drawing.Point(252, 95);
            this.lblMove.Name = "lblMove";
            this.lblMove.Size = new System.Drawing.Size(44, 13);
            this.lblMove.TabIndex = 8;
            this.lblMove.Text = "lblMove";
            // 
            // duplicateDataGridView
            // 
            this.duplicateDataGridView.AllowUserToAddRows = false;
            this.duplicateDataGridView.AllowUserToOrderColumns = true;
            this.duplicateDataGridView.AutoGenerateColumns = false;
            this.duplicateDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.duplicateDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.duplicateDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn1});
            this.duplicateDataGridView.DataSource = this.duplicateBindingSource;
            this.duplicateDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.duplicateDataGridView.Location = new System.Drawing.Point(0, 25);
            this.duplicateDataGridView.MultiSelect = false;
            this.duplicateDataGridView.Name = "duplicateDataGridView";
            this.duplicateDataGridView.RowHeadersVisible = false;
            this.duplicateDataGridView.RowTemplate.ReadOnly = true;
            this.duplicateDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.duplicateDataGridView.Size = new System.Drawing.Size(827, 347);
            this.duplicateDataGridView.TabIndex = 0;
            this.duplicateDataGridView.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.DuplicateDataGridView_RowPrePaint);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DupeNumber";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn2.HeaderText = "Dupe";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 39;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Size";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "n0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn4.HeaderText = "Size";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 33;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "FileName";
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn3.HeaderText = "FileName";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 57;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Directory";
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = "Directory";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // duplicateBindingSource
            // 
            this.duplicateBindingSource.DataSource = typeof(BruDupeFileRemover.Duplicate);
            // 
            // duplicateBindingNavigator
            // 
            this.duplicateBindingNavigator.AddNewItem = null;
            this.duplicateBindingNavigator.BindingSource = this.duplicateBindingSource;
            this.duplicateBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.duplicateBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.duplicateBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorDeleteItem});
            this.duplicateBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.duplicateBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.duplicateBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.duplicateBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.duplicateBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.duplicateBindingNavigator.Name = "duplicateBindingNavigator";
            this.duplicateBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.duplicateBindingNavigator.Size = new System.Drawing.Size(827, 25);
            this.duplicateBindingNavigator.TabIndex = 9;
            this.duplicateBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 520);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "BruDupeFileRemover";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.duplicateDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.duplicateBindingNavigator)).EndInit();
            this.duplicateBindingNavigator.ResumeLayout(false);
            this.duplicateBindingNavigator.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblScanRoot;
        private System.Windows.Forms.Button btnSetScanRoot;
        private System.Windows.Forms.Button btnSetDirectoryMoveRoot;
        private System.Windows.Forms.Label lblMoveRoot;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Label lblScan;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.BindingNavigator duplicateBindingNavigator;
        private System.Windows.Forms.BindingSource duplicateBindingSource;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.DataGridView duplicateDataGridView;
        private System.Windows.Forms.Label lblMove;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RichTextBox rtbExtraCopyStrings;
        private System.Windows.Forms.CheckBox chkBxAggressive;
    }
}

