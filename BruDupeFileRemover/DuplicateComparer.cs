﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;

namespace BruDupeFileRemover
{
    public class DuplicateComparer : IComparer<Duplicate>
    {
        public int Compare(Duplicate x, Duplicate y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null)
            {
                return -1;
            }
            if (y == null)
            {
                return 1;
            }
            var result = string.Compare(x.Directory, y.Directory, StringComparison.CurrentCulture);
            if (result == 0)
            {
                result = string.Compare(x.BaseName, y.BaseName, StringComparison.CurrentCulture);
            }
            if (result == 0)
            {
                result = x.Size.CompareTo(y.Size);
            }
            if (result == 0)
            {
                result = x.DupeNumber.CompareTo(y.DupeNumber);
            }
            return result;
        }
    }
}
