﻿//Copyright 2016 Dale A. Brubaker
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE    
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BruDupeFileRemover.ExtensionMethods;
using BruDupeFileRemover.Logging;
using BruDupeFileRemover.Properties;

namespace BruDupeFileRemover
{
    public partial class MainForm : Form
    {
        private static readonly ConfigBruDupeFileRemover Config = new ConfigBruDupeFileRemover(nameof(MainForm));
        private static readonly ILogger Log = new LoggerBruDupeFileRemover(typeof(MainForm));
        private CancellationTokenSource _cancellationTokenSource;
        private List<Duplicate> _duplicates;

        public MainForm()
        {
            InitializeComponent();
            lblMoveRoot.Text = "";
            lblScanRoot.Text = "";
            lblScan.Text = "";
            lblMove.Text = "";
            Log.WithCallInfo().Info("Starting MainForm");
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Log.WithCallInfo().Warn("Cancel requested!");
            _cancellationTokenSource.Cancel();
        }

        private async void BtnMove_ClickAsync(object sender, EventArgs e)
        {
            if (_duplicates == null || _duplicates.Count == 0)
            {
                SystemSounds.Exclamation.Play(); // beep a system sound so we know there's an error
                MessageBox.Show("You must scan first to detect duplicates.");
                return;
            }
            if (!MoveDespiteEmptyMoveDirectory())
            {
                return;
            }
            var saveText = btnMove.Text;
            btnMove.Enabled = false;
            btnScan.Enabled = false; // don't allow a scan during move
            btnMove.Text = Resources.Working;
            lblMove.Text = "";
            btnCancel.Visible = true;
            Update();
            await MoveAsync();

            // Empty the grid
            duplicateBindingSource.DataSource = new List<Duplicate>();
            duplicateBindingSource.ResetBindings(false);

            btnMove.Text = saveText;
            btnMove.Enabled = true;
            btnScan.Enabled = true;
            btnCancel.Visible = false;
        }

        private async void BtnScan_ClickAsync(object sender, EventArgs e)
        {
            var saveText = btnScan.Text;
            btnScan.Enabled = false;
            btnMove.Enabled = false; // don't allow a move during scan
            btnScan.Text = Resources.Working;
            lblMove.Text = "";
            btnCancel.Visible = true;
            Update();

            // Empty the grid
            _duplicates = new List<Duplicate>();
            duplicateBindingSource.DataSource = _duplicates;
            duplicateBindingSource.ResetBindings(false);
            _duplicates = (await ScanAsync()).ToList();
            if (_duplicates != null)
            {
                ShowDuplicates();
            }
            btnScan.Text = saveText;
            btnScan.Enabled = true;
            btnMove.Enabled = true;
            btnCancel.Visible = false;
        }

        private void BtnSetDirectoryMoveRoot_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = lblMoveRoot.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                lblMoveRoot.Text = folderBrowserDialog1.SelectedPath;
                CheckMoveDirectory();
            }
        }

        private void BtnSetScanRoot_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = lblScanRoot.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                lblScanRoot.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void CheckMoveDirectory()
        {
            if (!MoveFiles.DirectoryIsEmpty(lblMoveRoot.Text))
            {
                SystemSounds.Exclamation.Play(); // beep a system sound so we know there's an error
                MessageBox.Show(Resources.Move_directory_is_not_empty__It_should_be_empty_to_enable_easy_recovery_if_problems_occur, Resources.Files_are_in_Move_Directory);
            }
        }

        private void ChkBxAggressive_CheckedChanged(object sender, EventArgs e)
        {
            rtbExtraCopyStrings.Enabled = !chkBxAggressive.Checked;
        }

        private void DuplicateDataGridView_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            var dupeNumber = (int)duplicateDataGridView[0, e.RowIndex].Value;
            if (dupeNumber > 0)
            {
                duplicateDataGridView.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkRed;
            }
        }

        private void LoadConfig()
        {
            Config.Load();
            Config.RestoreWindow("Window", Handle);
            lblScanRoot.Text = Config.GetConfig("ScanRootPath", "");
            lblMoveRoot.Text = Config.GetConfig("MoveRootPath", "");
            chkBxAggressive.Checked = Config.GetConfig("Aggressive", false);
            var lines = Config.GetConfigEnumerable("ExtraCopyStrings", new string[0]).ToList();
            lines.RemoveAll(string.IsNullOrEmpty);
            rtbExtraCopyStrings.Lines = lines.ToArray();
            CheckMoveDirectory();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfig();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadConfig();
        }

        private async Task MoveAsync()
        {
            // We get total directories up front because we'll need them anyway, and we'll use those to report progress.
            // We do NOT get total files up front, because it takes much too long on a big directory.
            using (_cancellationTokenSource = new CancellationTokenSource())
            {
                var move = new MoveFiles(lblMoveRoot.Text, lblScanRoot.Text);
                lblMove.Text = "";
                var bytesMoved = 0L;
                var totalBytesToMove = _duplicates?.Sum(x => x.Size) ?? 0;
                var lastUpdate = DateTimeOffset.Now;
                var updateInterval = TimeSpan.FromMilliseconds(100);
                var onProgressNumBytesMoved = new Progress<long>(r =>
                {
                    bytesMoved += r;
                    if (DateTimeOffset.Now - lastUpdate > updateInterval)
                    {
                        lblMove.Text = $"Moved {bytesMoved.ToBytesPrettyFormat()} of {totalBytesToMove.ToBytesPrettyFormat()}";
                        lastUpdate = DateTimeOffset.Now;
                    }
                });
                var duplicatesToMove = _duplicates?.Where(d => d.DupeNumber > 0).ToList() ?? new List<Duplicate>();
                var ui = TaskScheduler.FromCurrentSynchronizationContext();
                var moveTask = Task.Run(
                    () => move.MoveDuplicates(duplicatesToMove, _cancellationTokenSource.Token, onProgressNumBytesMoved),
                    _cancellationTokenSource.Token);
                var displayResults = moveTask.ContinueWith(
                    resultTask => { Log.WithCallInfo().Info("Move completed successfully."); },
                    CancellationToken.None,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    ui);
                try
                {
                    await moveTask;
                    lblMove.Text = $"Moved {totalBytesToMove.ToBytesPrettyFormat()} of {totalBytesToMove.ToBytesPrettyFormat()}";
                }
                catch (AggregateException ae)
                {
                    ae.Flatten().Handle(ex =>
                    {
                        if (ex is OperationCanceledException)
                        {
                            // Here we just output a message and go on.
                            Log.WithCallInfo().Debug(ex.Message);
                            lblMove.Text = Resources.Cancelled;
                            return true;
                        }

                        // Handle other exceptions here if necessary...
                        Log.WithCallInfo().Error($"During move: {ex.Message}", ex);
                        return false;
                    });
                }
                catch (Exception ex)
                {
                    Log.WithCallInfo().Error($"During move: {ex.Message}", ex);
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private bool MoveDespiteEmptyMoveDirectory()
        {
            if (!MoveFiles.DirectoryIsEmpty(lblMoveRoot.Text))
            {
                SystemSounds.Exclamation.Play(); // beep a system sound so we know there's an error
                var dlgResult = MessageBox.Show(
                    Resources.Move_directory_is_not_empty__It_should_be_empty_to_enable_easy_recovery_if_problems_occur__Move_anyway_,
                    Resources.Files_are_in_Move_Directory, MessageBoxButtons.YesNo);
                return dlgResult == DialogResult.Yes;
            }
            return true;
        }

        private void SaveConfig()
        {
            Config.SaveWindow("Window", Handle);
            Config.SetConfig("ScanRootPath", lblScanRoot.Text);
            Config.SetConfig("MoveRootPath", lblMoveRoot.Text);
            Config.SetConfig("Aggressive", chkBxAggressive.Checked);
            Config.SetConfigEnumerable<string, string[]>("ExtraCopyStrings", rtbExtraCopyStrings.Lines);

            Config.Save();
        }

        private async Task<IReadOnlyCollection<Duplicate>> ScanActualDuplicatesAsync(int numDirectories, int numFiles, IEnumerable<IEnumerable<Duplicate>> potentialDuplicatesLists,
            Scan scan, TimeSpan updateInterval)
        {
            var actualDuplicatesFound = 0;
            var potentialDuplicatesProcessed = 0;
            var potentialDuplicatesBytesProcessed = 0L;
            var lastUpdate = DateTimeOffset.Now;
            var potentialDuplicatesTotalBytes = 0L; // excludes DupeNumber == 0 just as actuals will
            var duplicatesLists = potentialDuplicatesLists as IList<IEnumerable<Duplicate>> ?? potentialDuplicatesLists.ToList();
            foreach (var potentialDuplicatesList in duplicatesLists)
            {
                var duplicatesList = potentialDuplicatesList as IList<Duplicate> ?? potentialDuplicatesList.ToList();
                potentialDuplicatesTotalBytes += duplicatesList.Count * duplicatesList.First().Size;
            }
            var onProgressActualDuplicates = new Progress<ReportActualDuplicates>(r =>
            {
                actualDuplicatesFound += r.NumActualDuplicateFilesFound;
                potentialDuplicatesProcessed += r.NumPotentialDuplicateFilesProcessed;
                potentialDuplicatesBytesProcessed += r.NumPotentialDuplicateFileBytesProcessed;
                if (DateTimeOffset.Now - lastUpdate > updateInterval)
                {
                    lblScan.Text = $"Directories:{numDirectories:n0}, Files:{numFiles:n0}, Actual Duplicates:{actualDuplicatesFound:n0}, "
                                   + $" Potential Duplicates Processed:{potentialDuplicatesProcessed:n0} "
                                   + $"({potentialDuplicatesBytesProcessed.ToBytesPrettyFormat()}/{potentialDuplicatesTotalBytes.ToBytesPrettyFormat()}) )";
                    lastUpdate = DateTimeOffset.Now;
                }
            });
            var ui = TaskScheduler.FromCurrentSynchronizationContext();
            var actualDuplicatesTask = Task.Run(
                () => scan.GetActualDuplicatesParallel(duplicatesLists, _cancellationTokenSource.Token, onProgressActualDuplicates),
                _cancellationTokenSource.Token);
            var displayActualDuplicatesTaskResults = actualDuplicatesTask.ContinueWith(
                resultTask => { Log.WithCallInfo().Info($"Scan {nameof(Scan.GetActualDuplicatesParallel)} completed successfully."); },
                CancellationToken.None,
                TaskContinuationOptions.OnlyOnRanToCompletion,
                ui);
            var actualDuplicates = await actualDuplicatesTask;
            lblScan.Text = $"Directories:{numDirectories:n0}, Files:{numFiles:n0}, Actual Duplicates:{actualDuplicatesFound:n0}, "
                           + $" Potential Duplicates Processed:{potentialDuplicatesProcessed:n0} "
                           + $"({potentialDuplicatesBytesProcessed.ToBytesPrettyFormat()}/{potentialDuplicatesTotalBytes.ToBytesPrettyFormat()}) )";
            return actualDuplicates;
        }

        private async Task<IReadOnlyCollection<Duplicate>> ScanAsync()
        {
            // We get total directories up front because we'll need them anyway, and we'll use those to report progress.
            // We do NOT get total files up front, because it takes much too long on a big directory.
            using (_cancellationTokenSource = new CancellationTokenSource())
            {
                var scan = new Scan(lblScanRoot.Text);
                lblScan.Text = "";
                var updateInterval = TimeSpan.FromMilliseconds(100);

                try
                {
                    var directories = await ScanDirectoriesAsync(scan, updateInterval);
                    var tuple = await ScanPotentialDuplicatesAsync(directories, scan, updateInterval);
                    var numFiles = tuple.Item1;
                    var potentialDuplicatesLists = tuple.Item2;
                    var actualDuplicates = await ScanActualDuplicatesAsync(directories.Count, numFiles, potentialDuplicatesLists, scan, updateInterval);
                    return actualDuplicates;
                }
                catch (AggregateException ae)
                {
                    ae.Flatten().Handle(ex =>
                    {
                        if (ex is OperationCanceledException)
                        {
                            // Here we just output a message and go on.
                            Log.WithCallInfo().Debug(ex.Message);
                            lblScan.Text = Resources.Cancelled;
                            return true;
                        }

                        // Handle other exceptions here if necessary...
                        Log.WithCallInfo().Error($"During scan: {ex.Message}", ex);
                        return false;
                    });
                    return new List<Duplicate>();
                }
            }
        }

        private async Task<IReadOnlyCollection<string>> ScanDirectoriesAsync(Scan scan, TimeSpan updateInterval)
        {
            var directoriesFound = 1; // we start with scanRoot
            var lastUpdate = DateTimeOffset.Now;
            var onProgressNumDirectoriesFound = new Progress<int>(r =>
            {
                directoriesFound += r;
                if (DateTimeOffset.Now - lastUpdate > updateInterval)
                {
                    lblScan.Text = $"Directories: {directoriesFound:n0}";
                    lastUpdate = DateTimeOffset.Now;
                }
            });
            var uiContext = TaskScheduler.FromCurrentSynchronizationContext();
            var subDirs = new[] { lblScanRoot.Text };
            var directoriesTask = Task.Run(
                () => scan.GetDirectoriesParallel(subDirs, _cancellationTokenSource.Token, onProgressNumDirectoriesFound),
                _cancellationTokenSource.Token);
            var displayDirectoriesTaskResults = directoriesTask.ContinueWith(
                resultTask => { Log.WithCallInfo().Info($"Scan {nameof(Scan.GetDirectoriesParallel)} completed successfully."); },
                CancellationToken.None,
                TaskContinuationOptions.OnlyOnRanToCompletion,
                uiContext);
            var directories = await directoriesTask;
            lblScan.Text = $"Directories: {directories.Count:n0}";
            return directories;
        }

        /// <summary>
        ///     Returns a Tuple of NumFiles (the number of files found) and the list of lists of potential duplicate files
        /// </summary>
        /// <param name="directories"></param>
        /// <param name="scan"></param>
        /// <param name="updateInterval"></param>
        /// <returns></returns>
        private async Task<Tuple<int, IEnumerable<IEnumerable<Duplicate>>>> ScanPotentialDuplicatesAsync(IReadOnlyCollection<string> directories, Scan scan, TimeSpan updateInterval)
        {
            var numFiles = 0;
            var potentialDuplicatesFound = 0;
            var lastUpdate = DateTimeOffset.Now;
            var onProgressPotentialDuplicates = new Progress<int>(r =>
            {
                potentialDuplicatesFound += r;
                if (DateTimeOffset.Now - lastUpdate > updateInterval)
                {
                    lblScan.Text = $"Directories:{directories.Count:n0}, Files:{numFiles:n0}, Potential Duplicates:{potentialDuplicatesFound:n0}";
                    lastUpdate = DateTimeOffset.Now;
                }
            });
            var onProgressFiles = new Progress<int>(r =>
            {
                numFiles += r;
                if (DateTimeOffset.Now - lastUpdate > updateInterval)
                {
                    lblScan.Text = $"Directories:{directories.Count:n0}, Files:{numFiles:n0}, Potential Duplicates:{potentialDuplicatesFound:n0}";
                    lastUpdate = DateTimeOffset.Now;
                }
            });
            var ui = TaskScheduler.FromCurrentSynchronizationContext();
            var extraCopyStrings = new List<string>(rtbExtraCopyStrings.Lines);
            extraCopyStrings.RemoveAll(string.IsNullOrEmpty);
            var aggressive = chkBxAggressive.Checked;
            var potentialDuplicatesTask = Task.Run(
                () => scan.GetPotentialDuplicatesParallel(directories.ToList(), extraCopyStrings, aggressive, _cancellationTokenSource.Token,
                    onProgressPotentialDuplicates, onProgressFiles), _cancellationTokenSource.Token);
            var displayPotentialDuplicatesTaskResults = potentialDuplicatesTask.ContinueWith(
                resultTask => { Log.WithCallInfo().Info($"Scan {nameof(Scan.GetPotentialDuplicatesParallel)} completed successfully."); },
                CancellationToken.None,
                TaskContinuationOptions.OnlyOnRanToCompletion,
                ui);
            var potentialDuplicatesLists = await potentialDuplicatesTask;
            lblScan.Text = $"Directories:{directories.Count:n0}, Files:{numFiles:n0}, Potential Duplicates:{potentialDuplicatesFound:n0}";
            return new Tuple<int, IEnumerable<IEnumerable<Duplicate>>>(numFiles, potentialDuplicatesLists);
        }

        private void ShowDuplicates()
        {
            _duplicates.Sort(new DuplicateComparer());
            duplicateBindingSource.DataSource = _duplicates;
            duplicateBindingSource.ResetBindings(false);
            duplicateDataGridView.AllowUserToResizeColumns = true;

            // Now that the dataGridView has calculated the sizes for Dupe, Size and FileName, allow them to be resizeable
            var width = duplicateDataGridView.Columns[0].Width;
            duplicateDataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            duplicateDataGridView.Columns[0].Width = width;
            width = duplicateDataGridView.Columns[1].Width;
            duplicateDataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            duplicateDataGridView.Columns[1].Width = width;
            width = duplicateDataGridView.Columns[2].Width;
            duplicateDataGridView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            duplicateDataGridView.Columns[2].Width = width;
            Log.WithCallInfo().Info(lblScan.Text);
        }
    }
}